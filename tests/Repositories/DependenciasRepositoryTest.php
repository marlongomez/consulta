<?php namespace Tests\Repositories;

use App\Models\Dependencias;
use App\Repositories\DependenciasRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DependenciasRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DependenciasRepository
     */
    protected $dependenciasRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->dependenciasRepo = \App::make(DependenciasRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_dependencias()
    {
        $dependencias = Dependencias::factory()->make()->toArray();

        $createdDependencias = $this->dependenciasRepo->create($dependencias);

        $createdDependencias = $createdDependencias->toArray();
        $this->assertArrayHasKey('id', $createdDependencias);
        $this->assertNotNull($createdDependencias['id'], 'Created Dependencias must have id specified');
        $this->assertNotNull(Dependencias::find($createdDependencias['id']), 'Dependencias with given id must be in DB');
        $this->assertModelData($dependencias, $createdDependencias);
    }

    /**
     * @test read
     */
    public function test_read_dependencias()
    {
        $dependencias = Dependencias::factory()->create();

        $dbDependencias = $this->dependenciasRepo->find($dependencias->id_dependencia);

        $dbDependencias = $dbDependencias->toArray();
        $this->assertModelData($dependencias->toArray(), $dbDependencias);
    }

    /**
     * @test update
     */
    public function test_update_dependencias()
    {
        $dependencias = Dependencias::factory()->create();
        $fakeDependencias = Dependencias::factory()->make()->toArray();

        $updatedDependencias = $this->dependenciasRepo->update($fakeDependencias, $dependencias->id_dependencia);

        $this->assertModelData($fakeDependencias, $updatedDependencias->toArray());
        $dbDependencias = $this->dependenciasRepo->find($dependencias->id_dependencia);
        $this->assertModelData($fakeDependencias, $dbDependencias->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_dependencias()
    {
        $dependencias = Dependencias::factory()->create();

        $resp = $this->dependenciasRepo->delete($dependencias->id_dependencia);

        $this->assertTrue($resp);
        $this->assertNull(Dependencias::find($dependencias->id_dependencia), 'Dependencias should not exist in DB');
    }
}
