<?php namespace Tests\Repositories;

use App\Models\Bitacora_Prospecto;
use App\Repositories\Bitacora_ProspectoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Bitacora_ProspectoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Bitacora_ProspectoRepository
     */
    protected $bitacoraProspectoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->bitacoraProspectoRepo = \App::make(Bitacora_ProspectoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_bitacora__prospecto()
    {
        $bitacoraProspecto = Bitacora_Prospecto::factory()->make()->toArray();

        $createdBitacora_Prospecto = $this->bitacoraProspectoRepo->create($bitacoraProspecto);

        $createdBitacora_Prospecto = $createdBitacora_Prospecto->toArray();
        $this->assertArrayHasKey('id', $createdBitacora_Prospecto);
        $this->assertNotNull($createdBitacora_Prospecto['id'], 'Created Bitacora_Prospecto must have id specified');
        $this->assertNotNull(Bitacora_Prospecto::find($createdBitacora_Prospecto['id']), 'Bitacora_Prospecto with given id must be in DB');
        $this->assertModelData($bitacoraProspecto, $createdBitacora_Prospecto);
    }

    /**
     * @test read
     */
    public function test_read_bitacora__prospecto()
    {
        $bitacoraProspecto = Bitacora_Prospecto::factory()->create();

        $dbBitacora_Prospecto = $this->bitacoraProspectoRepo->find($bitacoraProspecto->id_bitacora_prospecto);

        $dbBitacora_Prospecto = $dbBitacora_Prospecto->toArray();
        $this->assertModelData($bitacoraProspecto->toArray(), $dbBitacora_Prospecto);
    }

    /**
     * @test update
     */
    public function test_update_bitacora__prospecto()
    {
        $bitacoraProspecto = Bitacora_Prospecto::factory()->create();
        $fakeBitacora_Prospecto = Bitacora_Prospecto::factory()->make()->toArray();

        $updatedBitacora_Prospecto = $this->bitacoraProspectoRepo->update($fakeBitacora_Prospecto, $bitacoraProspecto->id_bitacora_prospecto);

        $this->assertModelData($fakeBitacora_Prospecto, $updatedBitacora_Prospecto->toArray());
        $dbBitacora_Prospecto = $this->bitacoraProspectoRepo->find($bitacoraProspecto->id_bitacora_prospecto);
        $this->assertModelData($fakeBitacora_Prospecto, $dbBitacora_Prospecto->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_bitacora__prospecto()
    {
        $bitacoraProspecto = Bitacora_Prospecto::factory()->create();

        $resp = $this->bitacoraProspectoRepo->delete($bitacoraProspecto->id_bitacora_prospecto);

        $this->assertTrue($resp);
        $this->assertNull(Bitacora_Prospecto::find($bitacoraProspecto->id_bitacora_prospecto), 'Bitacora_Prospecto should not exist in DB');
    }
}
