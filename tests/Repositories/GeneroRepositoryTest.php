<?php namespace Tests\Repositories;

use App\Models\Genero;
use App\Repositories\GeneroRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class GeneroRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var GeneroRepository
     */
    protected $generoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->generoRepo = \App::make(GeneroRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_genero()
    {
        $genero = Genero::factory()->make()->toArray();

        $createdGenero = $this->generoRepo->create($genero);

        $createdGenero = $createdGenero->toArray();
        $this->assertArrayHasKey('id', $createdGenero);
        $this->assertNotNull($createdGenero['id'], 'Created Genero must have id specified');
        $this->assertNotNull(Genero::find($createdGenero['id']), 'Genero with given id must be in DB');
        $this->assertModelData($genero, $createdGenero);
    }

    /**
     * @test read
     */
    public function test_read_genero()
    {
        $genero = Genero::factory()->create();

        $dbGenero = $this->generoRepo->find($genero->id_genero);

        $dbGenero = $dbGenero->toArray();
        $this->assertModelData($genero->toArray(), $dbGenero);
    }

    /**
     * @test update
     */
    public function test_update_genero()
    {
        $genero = Genero::factory()->create();
        $fakeGenero = Genero::factory()->make()->toArray();

        $updatedGenero = $this->generoRepo->update($fakeGenero, $genero->id_genero);

        $this->assertModelData($fakeGenero, $updatedGenero->toArray());
        $dbGenero = $this->generoRepo->find($genero->id_genero);
        $this->assertModelData($fakeGenero, $dbGenero->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_genero()
    {
        $genero = Genero::factory()->create();

        $resp = $this->generoRepo->delete($genero->id_genero);

        $this->assertTrue($resp);
        $this->assertNull(Genero::find($genero->id_genero), 'Genero should not exist in DB');
    }
}
