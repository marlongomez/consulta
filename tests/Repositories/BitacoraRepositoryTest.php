<?php namespace Tests\Repositories;

use App\Models\Bitacora;
use App\Repositories\BitacoraRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BitacoraRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BitacoraRepository
     */
    protected $bitacoraRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->bitacoraRepo = \App::make(BitacoraRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_bitacora()
    {
        $bitacora = Bitacora::factory()->make()->toArray();

        $createdBitacora = $this->bitacoraRepo->create($bitacora);

        $createdBitacora = $createdBitacora->toArray();
        $this->assertArrayHasKey('id', $createdBitacora);
        $this->assertNotNull($createdBitacora['id'], 'Created Bitacora must have id specified');
        $this->assertNotNull(Bitacora::find($createdBitacora['id']), 'Bitacora with given id must be in DB');
        $this->assertModelData($bitacora, $createdBitacora);
    }

    /**
     * @test read
     */
    public function test_read_bitacora()
    {
        $bitacora = Bitacora::factory()->create();

        $dbBitacora = $this->bitacoraRepo->find($bitacora->id_bitacora);

        $dbBitacora = $dbBitacora->toArray();
        $this->assertModelData($bitacora->toArray(), $dbBitacora);
    }

    /**
     * @test update
     */
    public function test_update_bitacora()
    {
        $bitacora = Bitacora::factory()->create();
        $fakeBitacora = Bitacora::factory()->make()->toArray();

        $updatedBitacora = $this->bitacoraRepo->update($fakeBitacora, $bitacora->id_bitacora);

        $this->assertModelData($fakeBitacora, $updatedBitacora->toArray());
        $dbBitacora = $this->bitacoraRepo->find($bitacora->id_bitacora);
        $this->assertModelData($fakeBitacora, $dbBitacora->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_bitacora()
    {
        $bitacora = Bitacora::factory()->create();

        $resp = $this->bitacoraRepo->delete($bitacora->id_bitacora);

        $this->assertTrue($resp);
        $this->assertNull(Bitacora::find($bitacora->id_bitacora), 'Bitacora should not exist in DB');
    }
}
