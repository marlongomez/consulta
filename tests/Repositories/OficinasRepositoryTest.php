<?php namespace Tests\Repositories;

use App\Models\Oficinas;
use App\Repositories\OficinasRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OficinasRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OficinasRepository
     */
    protected $oficinasRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->oficinasRepo = \App::make(OficinasRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_oficinas()
    {
        $oficinas = Oficinas::factory()->make()->toArray();

        $createdOficinas = $this->oficinasRepo->create($oficinas);

        $createdOficinas = $createdOficinas->toArray();
        $this->assertArrayHasKey('id', $createdOficinas);
        $this->assertNotNull($createdOficinas['id'], 'Created Oficinas must have id specified');
        $this->assertNotNull(Oficinas::find($createdOficinas['id']), 'Oficinas with given id must be in DB');
        $this->assertModelData($oficinas, $createdOficinas);
    }

    /**
     * @test read
     */
    public function test_read_oficinas()
    {
        $oficinas = Oficinas::factory()->create();

        $dbOficinas = $this->oficinasRepo->find($oficinas->id_oficina);

        $dbOficinas = $dbOficinas->toArray();
        $this->assertModelData($oficinas->toArray(), $dbOficinas);
    }

    /**
     * @test update
     */
    public function test_update_oficinas()
    {
        $oficinas = Oficinas::factory()->create();
        $fakeOficinas = Oficinas::factory()->make()->toArray();

        $updatedOficinas = $this->oficinasRepo->update($fakeOficinas, $oficinas->id_oficina);

        $this->assertModelData($fakeOficinas, $updatedOficinas->toArray());
        $dbOficinas = $this->oficinasRepo->find($oficinas->id_oficina);
        $this->assertModelData($fakeOficinas, $dbOficinas->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_oficinas()
    {
        $oficinas = Oficinas::factory()->create();

        $resp = $this->oficinasRepo->delete($oficinas->id_oficina);

        $this->assertTrue($resp);
        $this->assertNull(Oficinas::find($oficinas->id_oficina), 'Oficinas should not exist in DB');
    }
}
