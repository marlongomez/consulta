<?php namespace Tests\Repositories;

use App\Models\Prospecto;
use App\Repositories\ProspectoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProspectoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProspectoRepository
     */
    protected $prospectoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->prospectoRepo = \App::make(ProspectoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_prospecto()
    {
        $prospecto = Prospecto::factory()->make()->toArray();

        $createdProspecto = $this->prospectoRepo->create($prospecto);

        $createdProspecto = $createdProspecto->toArray();
        $this->assertArrayHasKey('id', $createdProspecto);
        $this->assertNotNull($createdProspecto['id'], 'Created Prospecto must have id specified');
        $this->assertNotNull(Prospecto::find($createdProspecto['id']), 'Prospecto with given id must be in DB');
        $this->assertModelData($prospecto, $createdProspecto);
    }

    /**
     * @test read
     */
    public function test_read_prospecto()
    {
        $prospecto = Prospecto::factory()->create();

        $dbProspecto = $this->prospectoRepo->find($prospecto->id_prospecto);

        $dbProspecto = $dbProspecto->toArray();
        $this->assertModelData($prospecto->toArray(), $dbProspecto);
    }

    /**
     * @test update
     */
    public function test_update_prospecto()
    {
        $prospecto = Prospecto::factory()->create();
        $fakeProspecto = Prospecto::factory()->make()->toArray();

        $updatedProspecto = $this->prospectoRepo->update($fakeProspecto, $prospecto->id_prospecto);

        $this->assertModelData($fakeProspecto, $updatedProspecto->toArray());
        $dbProspecto = $this->prospectoRepo->find($prospecto->id_prospecto);
        $this->assertModelData($fakeProspecto, $dbProspecto->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_prospecto()
    {
        $prospecto = Prospecto::factory()->create();

        $resp = $this->prospectoRepo->delete($prospecto->id_prospecto);

        $this->assertTrue($resp);
        $this->assertNull(Prospecto::find($prospecto->id_prospecto), 'Prospecto should not exist in DB');
    }
}
