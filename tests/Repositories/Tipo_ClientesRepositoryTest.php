<?php namespace Tests\Repositories;

use App\Models\Tipo_Clientes;
use App\Repositories\Tipo_ClientesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Tipo_ClientesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Tipo_ClientesRepository
     */
    protected $tipoClientesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tipoClientesRepo = \App::make(Tipo_ClientesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tipo__clientes()
    {
        $tipoClientes = Tipo_Clientes::factory()->make()->toArray();

        $createdTipo_Clientes = $this->tipoClientesRepo->create($tipoClientes);

        $createdTipo_Clientes = $createdTipo_Clientes->toArray();
        $this->assertArrayHasKey('id', $createdTipo_Clientes);
        $this->assertNotNull($createdTipo_Clientes['id'], 'Created Tipo_Clientes must have id specified');
        $this->assertNotNull(Tipo_Clientes::find($createdTipo_Clientes['id']), 'Tipo_Clientes with given id must be in DB');
        $this->assertModelData($tipoClientes, $createdTipo_Clientes);
    }

    /**
     * @test read
     */
    public function test_read_tipo__clientes()
    {
        $tipoClientes = Tipo_Clientes::factory()->create();

        $dbTipo_Clientes = $this->tipoClientesRepo->find($tipoClientes->id_tipo_cliente);

        $dbTipo_Clientes = $dbTipo_Clientes->toArray();
        $this->assertModelData($tipoClientes->toArray(), $dbTipo_Clientes);
    }

    /**
     * @test update
     */
    public function test_update_tipo__clientes()
    {
        $tipoClientes = Tipo_Clientes::factory()->create();
        $fakeTipo_Clientes = Tipo_Clientes::factory()->make()->toArray();

        $updatedTipo_Clientes = $this->tipoClientesRepo->update($fakeTipo_Clientes, $tipoClientes->id_tipo_cliente);

        $this->assertModelData($fakeTipo_Clientes, $updatedTipo_Clientes->toArray());
        $dbTipo_Clientes = $this->tipoClientesRepo->find($tipoClientes->id_tipo_cliente);
        $this->assertModelData($fakeTipo_Clientes, $dbTipo_Clientes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tipo__clientes()
    {
        $tipoClientes = Tipo_Clientes::factory()->create();

        $resp = $this->tipoClientesRepo->delete($tipoClientes->id_tipo_cliente);

        $this->assertTrue($resp);
        $this->assertNull(Tipo_Clientes::find($tipoClientes->id_tipo_cliente), 'Tipo_Clientes should not exist in DB');
    }
}
