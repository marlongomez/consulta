<?php namespace Tests\Repositories;

use App\Models\Localidades;
use App\Repositories\LocalidadesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LocalidadesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LocalidadesRepository
     */
    protected $localidadesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->localidadesRepo = \App::make(LocalidadesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_localidades()
    {
        $localidades = Localidades::factory()->make()->toArray();

        $createdLocalidades = $this->localidadesRepo->create($localidades);

        $createdLocalidades = $createdLocalidades->toArray();
        $this->assertArrayHasKey('id', $createdLocalidades);
        $this->assertNotNull($createdLocalidades['id'], 'Created Localidades must have id specified');
        $this->assertNotNull(Localidades::find($createdLocalidades['id']), 'Localidades with given id must be in DB');
        $this->assertModelData($localidades, $createdLocalidades);
    }

    /**
     * @test read
     */
    public function test_read_localidades()
    {
        $localidades = Localidades::factory()->create();

        $dbLocalidades = $this->localidadesRepo->find($localidades->id_localidad);

        $dbLocalidades = $dbLocalidades->toArray();
        $this->assertModelData($localidades->toArray(), $dbLocalidades);
    }

    /**
     * @test update
     */
    public function test_update_localidades()
    {
        $localidades = Localidades::factory()->create();
        $fakeLocalidades = Localidades::factory()->make()->toArray();

        $updatedLocalidades = $this->localidadesRepo->update($fakeLocalidades, $localidades->id_localidad);

        $this->assertModelData($fakeLocalidades, $updatedLocalidades->toArray());
        $dbLocalidades = $this->localidadesRepo->find($localidades->id_localidad);
        $this->assertModelData($fakeLocalidades, $dbLocalidades->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_localidades()
    {
        $localidades = Localidades::factory()->create();

        $resp = $this->localidadesRepo->delete($localidades->id_localidad);

        $this->assertTrue($resp);
        $this->assertNull(Localidades::find($localidades->id_localidad), 'Localidades should not exist in DB');
    }
}
