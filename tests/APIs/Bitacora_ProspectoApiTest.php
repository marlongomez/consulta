<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Bitacora_Prospecto;

class Bitacora_ProspectoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_bitacora__prospecto()
    {
        $bitacoraProspecto = Bitacora_Prospecto::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/bitacora__prospectos', $bitacoraProspecto
        );

        $this->assertApiResponse($bitacoraProspecto);
    }

    /**
     * @test
     */
    public function test_read_bitacora__prospecto()
    {
        $bitacoraProspecto = Bitacora_Prospecto::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/bitacora__prospectos/'.$bitacoraProspecto->id_bitacora_prospecto
        );

        $this->assertApiResponse($bitacoraProspecto->toArray());
    }

    /**
     * @test
     */
    public function test_update_bitacora__prospecto()
    {
        $bitacoraProspecto = Bitacora_Prospecto::factory()->create();
        $editedBitacora_Prospecto = Bitacora_Prospecto::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/bitacora__prospectos/'.$bitacoraProspecto->id_bitacora_prospecto,
            $editedBitacora_Prospecto
        );

        $this->assertApiResponse($editedBitacora_Prospecto);
    }

    /**
     * @test
     */
    public function test_delete_bitacora__prospecto()
    {
        $bitacoraProspecto = Bitacora_Prospecto::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/bitacora__prospectos/'.$bitacoraProspecto->id_bitacora_prospecto
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/bitacora__prospectos/'.$bitacoraProspecto->id_bitacora_prospecto
        );

        $this->response->assertStatus(404);
    }
}
