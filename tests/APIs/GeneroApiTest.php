<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Genero;

class GeneroApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_genero()
    {
        $genero = Genero::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/generos', $genero
        );

        $this->assertApiResponse($genero);
    }

    /**
     * @test
     */
    public function test_read_genero()
    {
        $genero = Genero::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/generos/'.$genero->id_genero
        );

        $this->assertApiResponse($genero->toArray());
    }

    /**
     * @test
     */
    public function test_update_genero()
    {
        $genero = Genero::factory()->create();
        $editedGenero = Genero::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/generos/'.$genero->id_genero,
            $editedGenero
        );

        $this->assertApiResponse($editedGenero);
    }

    /**
     * @test
     */
    public function test_delete_genero()
    {
        $genero = Genero::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/generos/'.$genero->id_genero
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/generos/'.$genero->id_genero
        );

        $this->response->assertStatus(404);
    }
}
