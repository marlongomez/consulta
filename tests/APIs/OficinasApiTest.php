<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Oficinas;

class OficinasApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_oficinas()
    {
        $oficinas = Oficinas::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/oficinas', $oficinas
        );

        $this->assertApiResponse($oficinas);
    }

    /**
     * @test
     */
    public function test_read_oficinas()
    {
        $oficinas = Oficinas::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/oficinas/'.$oficinas->id_oficina
        );

        $this->assertApiResponse($oficinas->toArray());
    }

    /**
     * @test
     */
    public function test_update_oficinas()
    {
        $oficinas = Oficinas::factory()->create();
        $editedOficinas = Oficinas::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/oficinas/'.$oficinas->id_oficina,
            $editedOficinas
        );

        $this->assertApiResponse($editedOficinas);
    }

    /**
     * @test
     */
    public function test_delete_oficinas()
    {
        $oficinas = Oficinas::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/oficinas/'.$oficinas->id_oficina
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/oficinas/'.$oficinas->id_oficina
        );

        $this->response->assertStatus(404);
    }
}
