<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Localidades;

class LocalidadesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_localidades()
    {
        $localidades = Localidades::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/localidades', $localidades
        );

        $this->assertApiResponse($localidades);
    }

    /**
     * @test
     */
    public function test_read_localidades()
    {
        $localidades = Localidades::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/localidades/'.$localidades->id_localidad
        );

        $this->assertApiResponse($localidades->toArray());
    }

    /**
     * @test
     */
    public function test_update_localidades()
    {
        $localidades = Localidades::factory()->create();
        $editedLocalidades = Localidades::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/localidades/'.$localidades->id_localidad,
            $editedLocalidades
        );

        $this->assertApiResponse($editedLocalidades);
    }

    /**
     * @test
     */
    public function test_delete_localidades()
    {
        $localidades = Localidades::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/localidades/'.$localidades->id_localidad
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/localidades/'.$localidades->id_localidad
        );

        $this->response->assertStatus(404);
    }
}
