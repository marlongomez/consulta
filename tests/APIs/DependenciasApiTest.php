<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Dependencias;

class DependenciasApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_dependencias()
    {
        $dependencias = Dependencias::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/dependencias', $dependencias
        );

        $this->assertApiResponse($dependencias);
    }

    /**
     * @test
     */
    public function test_read_dependencias()
    {
        $dependencias = Dependencias::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/dependencias/'.$dependencias->id_dependencia
        );

        $this->assertApiResponse($dependencias->toArray());
    }

    /**
     * @test
     */
    public function test_update_dependencias()
    {
        $dependencias = Dependencias::factory()->create();
        $editedDependencias = Dependencias::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/dependencias/'.$dependencias->id_dependencia,
            $editedDependencias
        );

        $this->assertApiResponse($editedDependencias);
    }

    /**
     * @test
     */
    public function test_delete_dependencias()
    {
        $dependencias = Dependencias::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/dependencias/'.$dependencias->id_dependencia
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/dependencias/'.$dependencias->id_dependencia
        );

        $this->response->assertStatus(404);
    }
}
