<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Bitacora;

class BitacoraApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_bitacora()
    {
        $bitacora = Bitacora::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/bitacoras', $bitacora
        );

        $this->assertApiResponse($bitacora);
    }

    /**
     * @test
     */
    public function test_read_bitacora()
    {
        $bitacora = Bitacora::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/bitacoras/'.$bitacora->id_bitacora
        );

        $this->assertApiResponse($bitacora->toArray());
    }

    /**
     * @test
     */
    public function test_update_bitacora()
    {
        $bitacora = Bitacora::factory()->create();
        $editedBitacora = Bitacora::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/bitacoras/'.$bitacora->id_bitacora,
            $editedBitacora
        );

        $this->assertApiResponse($editedBitacora);
    }

    /**
     * @test
     */
    public function test_delete_bitacora()
    {
        $bitacora = Bitacora::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/bitacoras/'.$bitacora->id_bitacora
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/bitacoras/'.$bitacora->id_bitacora
        );

        $this->response->assertStatus(404);
    }
}
