<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Prospecto;

class ProspectoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_prospecto()
    {
        $prospecto = Prospecto::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/prospectos', $prospecto
        );

        $this->assertApiResponse($prospecto);
    }

    /**
     * @test
     */
    public function test_read_prospecto()
    {
        $prospecto = Prospecto::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/prospectos/'.$prospecto->id_prospecto
        );

        $this->assertApiResponse($prospecto->toArray());
    }

    /**
     * @test
     */
    public function test_update_prospecto()
    {
        $prospecto = Prospecto::factory()->create();
        $editedProspecto = Prospecto::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/prospectos/'.$prospecto->id_prospecto,
            $editedProspecto
        );

        $this->assertApiResponse($editedProspecto);
    }

    /**
     * @test
     */
    public function test_delete_prospecto()
    {
        $prospecto = Prospecto::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/prospectos/'.$prospecto->id_prospecto
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/prospectos/'.$prospecto->id_prospecto
        );

        $this->response->assertStatus(404);
    }
}
