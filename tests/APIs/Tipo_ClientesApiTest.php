<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Tipo_Clientes;

class Tipo_ClientesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tipo__clientes()
    {
        $tipoClientes = Tipo_Clientes::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/tipo__clientes', $tipoClientes
        );

        $this->assertApiResponse($tipoClientes);
    }

    /**
     * @test
     */
    public function test_read_tipo__clientes()
    {
        $tipoClientes = Tipo_Clientes::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/tipo__clientes/'.$tipoClientes->id_tipo_cliente
        );

        $this->assertApiResponse($tipoClientes->toArray());
    }

    /**
     * @test
     */
    public function test_update_tipo__clientes()
    {
        $tipoClientes = Tipo_Clientes::factory()->create();
        $editedTipo_Clientes = Tipo_Clientes::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/tipo__clientes/'.$tipoClientes->id_tipo_cliente,
            $editedTipo_Clientes
        );

        $this->assertApiResponse($editedTipo_Clientes);
    }

    /**
     * @test
     */
    public function test_delete_tipo__clientes()
    {
        $tipoClientes = Tipo_Clientes::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/tipo__clientes/'.$tipoClientes->id_tipo_cliente
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/tipo__clientes/'.$tipoClientes->id_tipo_cliente
        );

        $this->response->assertStatus(404);
    }
}
