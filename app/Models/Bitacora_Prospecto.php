<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Bitacora_Prospecto
 * @package App\Models
 * @version May 8, 2021, 2:59 pm EST
 *
 * @property integer $id_prospecto
 * @property string $fecha
 * @property string $observaciones
 */
class Bitacora_Prospecto extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'bitacora_prospecto';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_bitacora_prospecto';

    public $fillable = [
        'id_prospecto',
        'fecha',
        'observaciones'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_bitacora_prospecto' => 'integer',
        'id_prospecto' => 'integer',
        'fecha' => 'date',
        'observaciones' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
