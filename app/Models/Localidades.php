<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Localidades
 * @package App\Models
 * @version April 18, 2021, 11:15 am EST
 *
 * @property integer $id_municipio
 * @property string $nombre_localidad
 */
class Localidades extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'localidades';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_localidad';

    public $fillable = [
        'id_municipio',
        'nombre_localidad'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_localidad' => 'integer',
        'id_municipio' => 'integer',
        'nombre_localidad' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_municipio' => 'required',
        'nombre_localidad' => 'required'
    ];

    
}
