<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Tipo_Clientes
 * @package App\Models
 * @version April 18, 2021, 12:37 pm EST
 *
 * @property string $nombre_tipo
 */
class Tipo_Clientes extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'tipo_clientes';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_tipo_cliente';

    public $fillable = [
        'nombre_tipo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_tipo_cliente' => 'integer',
        'nombre_tipo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre_tipo' => 'required'
    ];

    
}
