<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Dependencias
 * @package App\Models
 * @version April 18, 2021, 11:37 am EST
 *
 * @property string $nombre_dependencia
 */
class Dependencias extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'dependencias';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_dependencia';

    public $fillable = [
        'nombre_dependencia'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_dependencia' => 'integer',
        'nombre_dependencia' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre_dependencia' => 'required'
    ];

    
}
