<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Oficinas
 * @package App\Models
 * @version April 18, 2021, 11:45 am EST
 *
 * @property integer $id_dependencia
 * @property string $nombre_oficina
 * @property string $direccion_oficina
 * @property string $apertura_oficina
 * @property string $cierre_oficina
 * @property string $observaciones
 */
class Oficinas extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'oficinas';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_oficina';

    public $fillable = [
        'id_dependencia',
        'nombre_oficina',
        'direccion_oficina',
        'apertura_oficina',
        'cierre_oficina',
        'observaciones',
        'id_municipio',
        'id_localidad'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_oficina' => 'integer',
        'id_dependencia' => 'integer',
        'nombre_oficina' => 'string',
        'direccion_oficina' => 'string',
        'apertura_oficina' => 'string',
        'cierre_oficina' => 'string',
        'observaciones' => 'string',
        'id_municipio' => 'integer',
        'id_localidad' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_dependencia' => 'required',
        'nombre_oficina' => 'required'
    ];

    
}
