<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Municipio
 * @package App\Models
 * @version April 18, 2021, 10:52 am EST
 *
 * @property string $nombre
 */
class Municipio extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'municipio';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_municipio';

    public $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_municipio' => 'integer',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
