<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Prospecto
 * @package App\Models
 * @version May 8, 2021, 1:55 pm EST
 *
 * @property string $nombre_prospecto
 * @property string $rfc_prospecto
 * @property string $telefono
 * @property integer $id_dependencia
 * @property integer $id_oficina
 * @property string $correo_electronico
 * @property integer $id_tipo_cliente
 * @property integer $id_municipio
 * @property integer $id_localidad
 */
class Prospecto extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'prospecto';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_prospecto';

    public $fillable = [
        'nombre_prospecto',
        'rfc_prospecto',
        'telefono',
        'id_dependencia',
        'id_oficina',
        'correo_electronico',
        'id_tipo_cliente',
        'id_municipio',
        'id_localidad'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_prospecto' => 'integer',
        'nombre_prospecto' => 'string',
        'rfc_prospecto' => 'string',
        'telefono' => 'string',
        'id_dependencia' => 'integer',
        'id_oficina' => 'integer',
        'correo_electronico' => 'string',
        'id_tipo_cliente' => 'integer',
        'id_municipio' => 'integer',
        'id_localidad' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre_prospecto' => 'required',
        'rfc_prospecto' => 'required',
        'id_dependencia' => 'required',
        'id_oficina' => 'required',
        'id_tipo_cliente' => 'required',
        'id_municipio' => 'required',
        'id_localidad' => 'required',
    ];

    
}
