<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Clientes
 * @package App\Models
 * @version April 18, 2021, 1:47 pm EST
 *
 * @property string $nombre_cliente
 * @property string $rfc_cliente
 * @property string $telefono
 * @property integer $id_dependencia
 * @property integer $id_oficina
 * @property string $correo_electronico
 * @property integer $id_tipo_cliente
 */
class Clientes extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'clientes';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_cliente';

    public $fillable = [
        'nombre_cliente',
        'rfc_cliente',
        'telefono',
        'id_dependencia',
        'id_oficina',
        'correo_electronico',
        'id_tipo_cliente',
        'id_municipio',
        'id_localidad',
        'agendado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_cliente' => 'integer',
        'nombre_cliente' => 'string',
        'rfc_cliente' => 'string',
        'telefono' => 'string',
        'id_dependencia' => 'integer',
        'id_oficina' => 'integer',
        'correo_electronico' => 'string',
        'id_tipo_cliente' => 'integer',
        'id_municipio' => 'integer',
        'id_localidad' => 'integer',
        'agendado' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre_cliente' => 'required',
        'rfc_cliente' => 'required',
        'id_dependencia' => 'required',
        'id_oficina' => 'required',
        'id_tipo_cliente' => 'required',
        'id_municipio' => 'required',
        'id_localidad' => 'required',
        'agendado'        => 'max:6',
    ];

    
}
