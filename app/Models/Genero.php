<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Genero
 * @package App\Models
 * @version April 13, 2021, 4:22 pm UTC
 *
 * @property string $nombre
 */
class Genero extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'genero';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_genero';

    public $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_genero' => 'integer',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required'
    ];

    
}
