<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Bitacora
 * @package App\Models
 * @version May 1, 2021, 12:07 pm EST
 *
 * @property integer $id_cliente
 * @property string $fecha
 * @property string $observaciones
 * @property string $ultima_venta
 */
class Bitacora extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'bitacora';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id_bitacora';

    public $fillable = [
        'id_cliente',
        'fecha',
        'observaciones',
        'ultima_venta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_bitacora' => 'integer',
        'id_cliente' => 'integer',
        'fecha' => 'date',
        'observaciones' => 'string',
        'ultima_venta' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ultima_venta'        => 'max:6'
    ];

    
}
