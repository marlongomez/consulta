<?php

namespace App\Repositories;

use App\Models\Localidades;
use App\Repositories\BaseRepository;

/**
 * Class LocalidadesRepository
 * @package App\Repositories
 * @version April 18, 2021, 11:15 am EST
*/

class LocalidadesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_municipio',
        'nombre_localidad'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Localidades::class;
    }
}
