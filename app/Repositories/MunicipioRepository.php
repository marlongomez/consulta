<?php

namespace App\Repositories;

use App\Models\Municipio;
use App\Repositories\BaseRepository;

/**
 * Class MunicipioRepository
 * @package App\Repositories
 * @version April 18, 2021, 10:52 am EST
*/

class MunicipioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Municipio::class;
    }
}
