<?php

namespace App\Repositories;

use App\Models\Dependencias;
use App\Repositories\BaseRepository;

/**
 * Class DependenciasRepository
 * @package App\Repositories
 * @version April 18, 2021, 11:37 am EST
*/

class DependenciasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_dependencia'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Dependencias::class;
    }
}
