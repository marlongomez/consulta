<?php

namespace App\Repositories;

use App\Models\Prospecto;
use App\Repositories\BaseRepository;

/**
 * Class ProspectoRepository
 * @package App\Repositories
 * @version May 8, 2021, 1:55 pm EST
*/

class ProspectoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_prospecto',
        'rfc_prospecto',
        'telefono',
        'id_dependencia',
        'id_oficina',
        'correo_electronico',
        'id_tipo_cliente',
        'id_municipio',
        'id_localidad'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Prospecto::class;
    }
}
