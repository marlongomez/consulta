<?php

namespace App\Repositories;

use App\Models\Bitacora;
use App\Repositories\BaseRepository;

/**
 * Class BitacoraRepository
 * @package App\Repositories
 * @version May 1, 2021, 12:07 pm EST
*/

class BitacoraRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_cliente',
        'fecha',
        'observaciones',
        'ultima_venta'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bitacora::class;
    }
}
