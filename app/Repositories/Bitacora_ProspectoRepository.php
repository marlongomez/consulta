<?php

namespace App\Repositories;

use App\Models\Bitacora_Prospecto;
use App\Repositories\BaseRepository;

/**
 * Class Bitacora_ProspectoRepository
 * @package App\Repositories
 * @version May 8, 2021, 2:59 pm EST
*/

class Bitacora_ProspectoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_prospecto',
        'fecha',
        'observaciones'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bitacora_Prospecto::class;
    }
}
