<?php

namespace App\Repositories;

use App\Models\Tipo_Clientes;
use App\Repositories\BaseRepository;

/**
 * Class Tipo_ClientesRepository
 * @package App\Repositories
 * @version April 18, 2021, 12:37 pm EST
*/

class Tipo_ClientesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tipo_Clientes::class;
    }
}
