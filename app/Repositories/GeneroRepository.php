<?php

namespace App\Repositories;

use App\Models\Genero;
use App\Repositories\BaseRepository;

/**
 * Class GeneroRepository
 * @package App\Repositories
 * @version April 13, 2021, 4:22 pm UTC
*/

class GeneroRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Genero::class;
    }
}
