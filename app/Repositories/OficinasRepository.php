<?php

namespace App\Repositories;

use App\Models\Oficinas;
use App\Repositories\BaseRepository;

/**
 * Class OficinasRepository
 * @package App\Repositories
 * @version April 18, 2021, 11:45 am EST
*/

class OficinasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_dependencia',
        'nombre_oficina',
        'direccion_oficina',
        'apertura_oficina',
        'cierre_oficina',
        'observaciones'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Oficinas::class;
    }
}
