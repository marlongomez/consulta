<?php

namespace App\Http\Controllers;

use App\DataTables\DependenciasDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDependenciasRequest;
use App\Http\Requests\UpdateDependenciasRequest;
use App\Repositories\DependenciasRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DependenciasController extends AppBaseController
{
    /** @var  DependenciasRepository */
    private $dependenciasRepository;

    public function __construct(DependenciasRepository $dependenciasRepo)
    {
        $this->dependenciasRepository = $dependenciasRepo;
    }

    /**
     * Display a listing of the Dependencias.
     *
     * @param DependenciasDataTable $dependenciasDataTable
     * @return Response
     */
    public function index(DependenciasDataTable $dependenciasDataTable)
    {
        return $dependenciasDataTable->render('dependencias.index');
    }

    /**
     * Show the form for creating a new Dependencias.
     *
     * @return Response
     */
    public function create()
    {
        return view('dependencias.create');
    }

    /**
     * Store a newly created Dependencias in storage.
     *
     * @param CreateDependenciasRequest $request
     *
     * @return Response
     */
    public function store(CreateDependenciasRequest $request)
    {
        $input = $request->all();

        $dependencias = $this->dependenciasRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/dependencias.singular')]));

        return redirect(route('dependencias.index'));
    }

    /**
     * Display the specified Dependencias.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dependencias = $this->dependenciasRepository->find($id);

        if (empty($dependencias)) {
            Flash::error(__('messages.not_found', ['model' => __('models/dependencias.singular')]));

            return redirect(route('dependencias.index'));
        }

        return view('dependencias.show')->with('dependencias', $dependencias);
    }

    /**
     * Show the form for editing the specified Dependencias.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dependencias = $this->dependenciasRepository->find($id);

        if (empty($dependencias)) {
            Flash::error(__('messages.not_found', ['model' => __('models/dependencias.singular')]));

            return redirect(route('dependencias.index'));
        }

        return view('dependencias.edit')->with('dependencias', $dependencias);
    }

    /**
     * Update the specified Dependencias in storage.
     *
     * @param  int              $id
     * @param UpdateDependenciasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDependenciasRequest $request)
    {
        $dependencias = $this->dependenciasRepository->find($id);

        if (empty($dependencias)) {
            Flash::error(__('messages.not_found', ['model' => __('models/dependencias.singular')]));

            return redirect(route('dependencias.index'));
        }

        $dependencias = $this->dependenciasRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/dependencias.singular')]));

        return redirect(route('dependencias.index'));
    }

    /**
     * Remove the specified Dependencias from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dependencias = $this->dependenciasRepository->find($id);

        if (empty($dependencias)) {
            Flash::error(__('messages.not_found', ['model' => __('models/dependencias.singular')]));

            return redirect(route('dependencias.index'));
        }

        $this->dependenciasRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/dependencias.singular')]));

        return redirect(route('dependencias.index'));
    }
}
