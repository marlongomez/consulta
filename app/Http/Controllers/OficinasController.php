<?php

namespace App\Http\Controllers;

use App\DataTables\OficinasDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOficinasRequest;
use App\Http\Requests\UpdateOficinasRequest;
use App\Repositories\OficinasRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Localidades;

class OficinasController extends AppBaseController
{
    /** @var  OficinasRepository */
    private $oficinasRepository;

    public function __construct(OficinasRepository $oficinasRepo)
    {
        $this->oficinasRepository = $oficinasRepo;
    }

    /**
     * Display a listing of the Oficinas.
     *
     * @param OficinasDataTable $oficinasDataTable
     * @return Response
     */
    public function index(OficinasDataTable $oficinasDataTable)
    {
        return $oficinasDataTable->render('oficinas.index');
    }

    /**
     * Show the form for creating a new Oficinas.
     *
     * @return Response
     */
    public function create()
    {
        return view('oficinas.create');
    }

    /**
     * Store a newly created Oficinas in storage.
     *
     * @param CreateOficinasRequest $request
     *
     * @return Response
     */
    public function store(CreateOficinasRequest $request)
    {
        $input = $request->all();

        $oficinas = $this->oficinasRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/oficinas.singular')]));

        return redirect(route('oficinas.index'));
    }

    /**
     * Display the specified Oficinas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $oficinas = $this->oficinasRepository->find($id);

        if (empty($oficinas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/oficinas.singular')]));

            return redirect(route('oficinas.index'));
        }

        return view('oficinas.show')->with('oficinas', $oficinas);
    }

    /**
     * Show the form for editing the specified Oficinas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $oficinas = $this->oficinasRepository->find($id);

        if (empty($oficinas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/oficinas.singular')]));

            return redirect(route('oficinas.index'));
        }

        return view('oficinas.edit')->with('oficinas', $oficinas);
    }

    /**
     * Update the specified Oficinas in storage.
     *
     * @param  int              $id
     * @param UpdateOficinasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOficinasRequest $request)
    {
        $oficinas = $this->oficinasRepository->find($id);

        if (empty($oficinas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/oficinas.singular')]));

            return redirect(route('oficinas.index'));
        }

        $oficinas = $this->oficinasRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/oficinas.singular')]));

        return redirect(route('oficinas.index'));
    }

    /**
     * Remove the specified Oficinas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $oficinas = $this->oficinasRepository->find($id);

        if (empty($oficinas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/oficinas.singular')]));

            return redirect(route('oficinas.index'));
        }

        $this->oficinasRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/oficinas.singular')]));

        return redirect(route('oficinas.index'));
    }

    public function getLocalidades($id_mun)
    {
        $municipios = Localidades::where('id_municipio', $id_mun)->pluck('nombre_localidad', 'id_localidad');
        $list = '';
        foreach ($municipios as $key => $value) {
            $list .= "<option value='" . $key . "'>" . $value . "</option>";
        }
        return $list;
    }

}
