<?php

namespace App\Http\Controllers;

use App\DataTables\Bitacora_ProspectoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBitacora_ProspectoRequest;
use App\Http\Requests\UpdateBitacora_ProspectoRequest;
use App\Repositories\Bitacora_ProspectoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Bitacora_ProspectoController extends AppBaseController
{
    /** @var  Bitacora_ProspectoRepository */
    private $bitacoraProspectoRepository;

    public function __construct(Bitacora_ProspectoRepository $bitacoraProspectoRepo)
    {
        $this->bitacoraProspectoRepository = $bitacoraProspectoRepo;
    }

    /**
     * Display a listing of the Bitacora_Prospecto.
     *
     * @param Bitacora_ProspectoDataTable $bitacoraProspectoDataTable
     * @return Response
     */
    public function index($id_prospecto, Bitacora_ProspectoDataTable $bitacoraProspectoDataTable)
    {
        $id_prospecto = decrypt($id_prospecto);
        

        $prospecto=\DB::table('prospecto')
            ->select('prospecto.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente')
            ->join('dependencias','dependencias.id_dependencia','=','prospecto.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','prospecto.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','prospecto.id_tipo_cliente')
            ->where("id_prospecto", "=", $id_prospecto)
            ->get()
            ->all();

        //dd($prospecto);

        return $bitacoraProspectoDataTable->with('id_prospecto', $id_prospecto)->render('bitacora__prospectos.index',['prospecto'=>$prospecto]);
    }

    /**
     * Show the form for creating a new Bitacora_Prospecto.
     *
     * @return Response
     */
    public function create($id_prospecto)
    {
        $id_prospecto = decrypt($id_prospecto);

        //dd($id_prospecto);

        $prospecto=\DB::table('prospecto')
            ->select('prospecto.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente')
            ->join('dependencias','dependencias.id_dependencia','=','prospecto.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','prospecto.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','prospecto.id_tipo_cliente')
            ->where("id_prospecto", "=", $id_prospecto)
            ->get()
            ->all();

        //dd($prospecto);
        
        return view('bitacora__prospectos.create')->with('prospecto', $prospecto);
    }

    /**
     * Store a newly created Bitacora_Prospecto in storage.
     *
     * @param CreateBitacora_ProspectoRequest $request
     *
     * @return Response
     */
    public function store(CreateBitacora_ProspectoRequest $request)
    {
        $input = $request->all();

        $id_prospecto=$input['id_prospecto'];

        $bitacoraProspecto = $this->bitacoraProspectoRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/bitacoraProspectos.singular')]));

        return redirect(route('bitacoraProspectos.index',encrypt($id_prospecto)));
    }

    /**
     * Display the specified Bitacora_Prospecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bitacoraProspecto = $this->bitacoraProspectoRepository->find($id);

        if (empty($bitacoraProspecto)) {
            Flash::error(__('messages.not_found', ['model' => __('models/bitacoraProspectos.singular')]));

            return redirect(route('bitacoraProspectos.index'));
        }

        return view('bitacora__prospectos.show')->with('bitacoraProspecto', $bitacoraProspecto);
    }

    /**
     * Show the form for editing the specified Bitacora_Prospecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $bitacoraProspecto = $this->bitacoraProspectoRepository->find($id);

        $id_prospecto=$bitacoraProspecto->id_prospecto;

        $prospecto=\DB::table('prospecto')
            ->select('prospecto.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente')
            ->join('dependencias','dependencias.id_dependencia','=','prospecto.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','prospecto.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','prospecto.id_tipo_cliente')
            ->where("id_prospecto", "=", $id_prospecto)
            ->get()
            ->all();

        //dd($bitacoraProspecto);

        if (empty($bitacoraProspecto)) {
            Flash::error(__('messages.not_found', ['model' => __('models/bitacoraProspectos.singular')]));

            return redirect(route('bitacoraProspectos.index'));
        }

        return view('bitacora__prospectos.edit')->with('bitacoraProspecto', $bitacoraProspecto)->with('prospecto', $prospecto);
    }

    /**
     * Update the specified Bitacora_Prospecto in storage.
     *
     * @param  int              $id
     * @param UpdateBitacora_ProspectoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBitacora_ProspectoRequest $request)
    {
        $bitacoraProspecto = $this->bitacoraProspectoRepository->find($id);

        $input = $request->all();
        $id_prospecto=$input['id_prospecto'];

        if (empty($bitacoraProspecto)) {
            Flash::error(__('messages.not_found', ['model' => __('models/bitacoraProspectos.singular')]));

            return redirect(route('bitacoraProspectos.index'));
        }

        $bitacoraProspecto = $this->bitacoraProspectoRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/bitacoraProspectos.singular')]));

        return redirect(route('bitacoraProspectos.index',encrypt($id_prospecto)));
    }

    /**
     * Remove the specified Bitacora_Prospecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bitacoraProspecto = $this->bitacoraProspectoRepository->find($id);

        //dd($bitacoraProspecto);

        $id_prospecto=$bitacoraProspecto->id_prospecto;

        if (empty($bitacoraProspecto)) {
            Flash::error(__('messages.not_found', ['model' => __('models/bitacoraProspectos.singular')]));

            return redirect(route('bitacoraProspectos.index'));
        }

        $this->bitacoraProspectoRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/bitacoraProspectos.singular')]));

        return redirect(route('bitacoraProspectos.index',encrypt($id_prospecto)));
    }
}
