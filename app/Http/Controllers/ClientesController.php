<?php

namespace App\Http\Controllers;

use App\DataTables\ClientesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateClientesRequest;
use App\Http\Requests\UpdateClientesRequest;
use App\Repositories\ClientesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Clientes;

class ClientesController extends AppBaseController
{
    /** @var  ClientesRepository */
    private $clientesRepository;

    public function __construct(ClientesRepository $clientesRepo)
    {
        $this->clientesRepository = $clientesRepo;
    }

    /**
     * Display a listing of the Clientes.
     *
     * @param ClientesDataTable $clientesDataTable
     * @return Response
     */
    public function index(ClientesDataTable $clientesDataTable)
    {
        return $clientesDataTable->render('clientes.index');
    }

    /**
     * Show the form for creating a new Clientes.
     *
     * @return Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created Clientes in storage.
     *
     * @param CreateClientesRequest $request
     *
     * @return Response
     */
    public function store(CreateClientesRequest $request)
    {

        //consultando rfc insertado
        $rfc =$request->rfc_cliente;
        //buscando si se encuentra dentro del sistema
        $findrfc = Clientes::where("rfc_cliente", "=", $rfc)->get()->toArray();

        //dd($findrfc);
        if (empty($findrfc)) {
            //si no encuentra el cliente lo inserta
            $input = $request->all();

            $clientes = $this->clientesRepository->create($input);

            Flash::success(__('messages.saved', ['model' => __('models/clientes.singular')]));

            return redirect(route('clientes.index'));

        }else{
            //si lo encuentra manda un mensaje de que se encuentra en el sistema
            Flash::error('El cliente ya se encuentra registrado');
            return redirect(route('clientes.create'));
        }

    }

    /**
     * Display the specified Clientes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $id = decrypt($id);
        //$clientes = $this->clientesRepository->find($id);
        $clientes=\DB::table('clientes')
            ->select('clientes.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente','municipio.nombre as municipio', 'localidades.nombre_localidad as localidad')
            ->join('dependencias','dependencias.id_dependencia','=','clientes.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','clientes.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','clientes.id_tipo_cliente')
            ->join('municipio','municipio.id_municipio','=','clientes.id_municipio')
            ->join('localidades','localidades.id_localidad','=','clientes.id_localidad')
            ->where("id_cliente", "=", $id)
            ->get()
            ->all();

        //dd($clientes);


        if (empty($clientes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/clientes.singular')]));

            return redirect(route('clientes.index'));
        }

        return view('clientes.show')->with('clientes', $clientes);
    }

    /**
     * Show the form for editing the specified Clientes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $clientes = $this->clientesRepository->find($id);

        if (empty($clientes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/clientes.singular')]));

            return redirect(route('clientes.index'));
        }

        return view('clientes.edit')->with('clientes', $clientes);
    }

    /**
     * Update the specified Clientes in storage.
     *
     * @param  int              $id
     * @param UpdateClientesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientesRequest $request)
    {
        $clientes = $this->clientesRepository->find($id);

        if (empty($clientes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/clientes.singular')]));

            return redirect(route('clientes.index'));
        }

        $clientes = $this->clientesRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/clientes.singular')]));

        return redirect(route('clientes.index'));
    }

    /**
     * Remove the specified Clientes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clientes = $this->clientesRepository->find($id);

        if (empty($clientes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/clientes.singular')]));

            return redirect(route('clientes.index'));
        }

        $this->clientesRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/clientes.singular')]));

        return redirect(route('clientes.index'));
    }
}
