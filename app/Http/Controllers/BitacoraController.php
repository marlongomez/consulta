<?php

namespace App\Http\Controllers;

use App\DataTables\BitacoraDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBitacoraRequest;
use App\Http\Requests\UpdateBitacoraRequest;
use App\Repositories\BitacoraRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class BitacoraController extends AppBaseController
{
    /** @var  BitacoraRepository */
    private $bitacoraRepository;

    public function __construct(BitacoraRepository $bitacoraRepo)
    {
        $this->bitacoraRepository = $bitacoraRepo;
    }

    /**
     * Display a listing of the Bitacora.
     *
     * @param BitacoraDataTable $bitacoraDataTable
     * @return Response
     */
    public function index($id_cliente,BitacoraDataTable $bitacoraDataTable)
    {
        //dd($id_cliente);
        $id_cliente = decrypt($id_cliente);
        
        $clientes=\DB::table('clientes')
            ->select('clientes.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente','municipio.nombre as municipio', 'localidades.nombre_localidad as localidad')
            ->join('dependencias','dependencias.id_dependencia','=','clientes.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','clientes.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','clientes.id_tipo_cliente')
            ->join('municipio','municipio.id_municipio','=','clientes.id_municipio')
            ->join('localidades','localidades.id_localidad','=','clientes.id_localidad')
            ->where("id_cliente", "=", $id_cliente)
            ->get()
            ->all();

        return $bitacoraDataTable->with('id_cliente', $id_cliente)->render('bitacoras.index',['clientes'=>$clientes]);
    }

    /**
     * Show the form for creating a new Bitacora.
     *
     * @return Response
     */
    public function create($id_cliente)
    {
        $id_cliente = decrypt($id_cliente);
        //dd($id_cliente);

        $clientes=\DB::table('clientes')
            ->select('clientes.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente','municipio.nombre as municipio', 'localidades.nombre_localidad as localidad')
            ->join('dependencias','dependencias.id_dependencia','=','clientes.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','clientes.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','clientes.id_tipo_cliente')
            ->join('municipio','municipio.id_municipio','=','clientes.id_municipio')
            ->join('localidades','localidades.id_localidad','=','clientes.id_localidad')
            ->where("id_cliente", "=", $id_cliente)
            ->get()
            ->all();

        //dd($clientes);

        return view('bitacoras.create')->with('clientes', $clientes);
    }

    /**
     * Store a newly created Bitacora in storage.
     *
     * @param CreateBitacoraRequest $request
     *
     * @return Response
     */
    public function store(CreateBitacoraRequest $request)
    {

        $validatedData = $request->validate([
                'ultima_venta' =>'max:6',
                'agendado' =>'max:6'
            ]);

        //dd($validatedData);

        $input = $request->all();

        $agendado=$input['agendado'];
        $id_cliente=$input['id_cliente'];

        $bitacora = $this->bitacoraRepository->create($input);

        \DB::table('clientes')
            ->where('clientes.id_cliente', '=', $id_cliente)
            ->update(['clientes.agendado' => $agendado]);

        Flash::success(__('messages.saved', ['model' => __('models/bitacoras.singular')]));

        return redirect(route('bitacoras.index',encrypt($id_cliente)));
    }

    /**
     * Display the specified Bitacora.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bitacora = $this->bitacoraRepository->find($id);

        if (empty($bitacora)) {
            Flash::error(__('messages.not_found', ['model' => __('models/bitacoras.singular')]));

            return redirect(route('bitacoras.index'));
        }

        return view('bitacoras.show')->with('bitacora', $bitacora);
    }

    /**
     * Show the form for editing the specified Bitacora.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //dd($id);
        $bitacora = $this->bitacoraRepository->find($id);

        $id_cliente=$bitacora->id_cliente;

        $clientes=\DB::table('clientes')
            ->select('clientes.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente')
            ->join('dependencias','dependencias.id_dependencia','=','clientes.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','clientes.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','clientes.id_tipo_cliente')
            ->where("id_cliente", "=", $id_cliente)
            ->get()
            ->all();

        

        if (empty($bitacora)) {
            Flash::error(__('messages.not_found', ['model' => __('models/bitacoras.singular')]));

            return redirect(route('bitacoras.index'));
        }

        return view('bitacoras.edit')->with('bitacora', $bitacora)->with('clientes', $clientes);
    }

    /**
     * Update the specified Bitacora in storage.
     *
     * @param  int              $id
     * @param UpdateBitacoraRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBitacoraRequest $request)
    {
        $bitacora = $this->bitacoraRepository->find($id);
        //obteniendo el id_cliente
         $id_cliente=$bitacora->id_cliente;

        //obteniendo el agendado
         $input = $request->all();
         $agendado=$input['agendado'];


        if (empty($bitacora)) {
            Flash::error(__('messages.not_found', ['model' => __('models/bitacoras.singular')]));

            return redirect(route('bitacoras.index'));
        }

        $bitacora = $this->bitacoraRepository->update($request->all(), $id);
        //actualizando el dato de agendado
        \DB::table('clientes')
            ->where('clientes.id_cliente', '=', $id_cliente)
            ->update(['clientes.agendado' => $agendado]);

        Flash::success(__('messages.updated', ['model' => __('models/bitacoras.singular')]));

        return redirect(route('bitacoras.index',encrypt($id_cliente)));
    }

    /**
     * Remove the specified Bitacora from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bitacora = $this->bitacoraRepository->find($id);

        $id_cliente=$bitacora->id_cliente;

        if (empty($bitacora)) {
            Flash::error(__('messages.not_found', ['model' => __('models/bitacoras.singular')]));

            return redirect(route('bitacoras.index'));
        }

        $this->bitacoraRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/bitacoras.singular')]));

        return redirect(route('bitacoras.index',encrypt($id_cliente)));
    }
}
