<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLocalidadesAPIRequest;
use App\Http\Requests\API\UpdateLocalidadesAPIRequest;
use App\Models\Localidades;
use App\Repositories\LocalidadesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LocalidadesController
 * @package App\Http\Controllers\API
 */

class LocalidadesAPIController extends AppBaseController
{
    /** @var  LocalidadesRepository */
    private $localidadesRepository;

    public function __construct(LocalidadesRepository $localidadesRepo)
    {
        $this->localidadesRepository = $localidadesRepo;
    }

    /**
     * Display a listing of the Localidades.
     * GET|HEAD /localidades
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $localidades = $this->localidadesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $localidades->toArray(),
            __('messages.retrieved', ['model' => __('models/localidades.plural')])
        );
    }

    /**
     * Store a newly created Localidades in storage.
     * POST /localidades
     *
     * @param CreateLocalidadesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLocalidadesAPIRequest $request)
    {
        $input = $request->all();

        $localidades = $this->localidadesRepository->create($input);

        return $this->sendResponse(
            $localidades->toArray(),
            __('messages.saved', ['model' => __('models/localidades.singular')])
        );
    }

    /**
     * Display the specified Localidades.
     * GET|HEAD /localidades/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Localidades $localidades */
        $localidades = $this->localidadesRepository->find($id);

        if (empty($localidades)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/localidades.singular')])
            );
        }

        return $this->sendResponse(
            $localidades->toArray(),
            __('messages.retrieved', ['model' => __('models/localidades.singular')])
        );
    }

    /**
     * Update the specified Localidades in storage.
     * PUT/PATCH /localidades/{id}
     *
     * @param int $id
     * @param UpdateLocalidadesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLocalidadesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Localidades $localidades */
        $localidades = $this->localidadesRepository->find($id);

        if (empty($localidades)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/localidades.singular')])
            );
        }

        $localidades = $this->localidadesRepository->update($input, $id);

        return $this->sendResponse(
            $localidades->toArray(),
            __('messages.updated', ['model' => __('models/localidades.singular')])
        );
    }

    /**
     * Remove the specified Localidades from storage.
     * DELETE /localidades/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Localidades $localidades */
        $localidades = $this->localidadesRepository->find($id);

        if (empty($localidades)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/localidades.singular')])
            );
        }

        $localidades->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/localidades.singular')])
        );
    }
}
