<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProspectoAPIRequest;
use App\Http\Requests\API\UpdateProspectoAPIRequest;
use App\Models\Prospecto;
use App\Repositories\ProspectoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProspectoController
 * @package App\Http\Controllers\API
 */

class ProspectoAPIController extends AppBaseController
{
    /** @var  ProspectoRepository */
    private $prospectoRepository;

    public function __construct(ProspectoRepository $prospectoRepo)
    {
        $this->prospectoRepository = $prospectoRepo;
    }

    /**
     * Display a listing of the Prospecto.
     * GET|HEAD /prospectos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $prospectos = $this->prospectoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $prospectos->toArray(),
            __('messages.retrieved', ['model' => __('models/prospectos.plural')])
        );
    }

    /**
     * Store a newly created Prospecto in storage.
     * POST /prospectos
     *
     * @param CreateProspectoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProspectoAPIRequest $request)
    {
        $input = $request->all();

        $prospecto = $this->prospectoRepository->create($input);

        return $this->sendResponse(
            $prospecto->toArray(),
            __('messages.saved', ['model' => __('models/prospectos.singular')])
        );
    }

    /**
     * Display the specified Prospecto.
     * GET|HEAD /prospectos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Prospecto $prospecto */
        $prospecto = $this->prospectoRepository->find($id);

        if (empty($prospecto)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/prospectos.singular')])
            );
        }

        return $this->sendResponse(
            $prospecto->toArray(),
            __('messages.retrieved', ['model' => __('models/prospectos.singular')])
        );
    }

    /**
     * Update the specified Prospecto in storage.
     * PUT/PATCH /prospectos/{id}
     *
     * @param int $id
     * @param UpdateProspectoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProspectoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Prospecto $prospecto */
        $prospecto = $this->prospectoRepository->find($id);

        if (empty($prospecto)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/prospectos.singular')])
            );
        }

        $prospecto = $this->prospectoRepository->update($input, $id);

        return $this->sendResponse(
            $prospecto->toArray(),
            __('messages.updated', ['model' => __('models/prospectos.singular')])
        );
    }

    /**
     * Remove the specified Prospecto from storage.
     * DELETE /prospectos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Prospecto $prospecto */
        $prospecto = $this->prospectoRepository->find($id);

        if (empty($prospecto)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/prospectos.singular')])
            );
        }

        $prospecto->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/prospectos.singular')])
        );
    }
}
