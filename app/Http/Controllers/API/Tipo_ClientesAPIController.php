<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTipo_ClientesAPIRequest;
use App\Http\Requests\API\UpdateTipo_ClientesAPIRequest;
use App\Models\Tipo_Clientes;
use App\Repositories\Tipo_ClientesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Tipo_ClientesController
 * @package App\Http\Controllers\API
 */

class Tipo_ClientesAPIController extends AppBaseController
{
    /** @var  Tipo_ClientesRepository */
    private $tipoClientesRepository;

    public function __construct(Tipo_ClientesRepository $tipoClientesRepo)
    {
        $this->tipoClientesRepository = $tipoClientesRepo;
    }

    /**
     * Display a listing of the Tipo_Clientes.
     * GET|HEAD /tipoClientes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tipoClientes = $this->tipoClientesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $tipoClientes->toArray(),
            __('messages.retrieved', ['model' => __('models/tipoClientes.plural')])
        );
    }

    /**
     * Store a newly created Tipo_Clientes in storage.
     * POST /tipoClientes
     *
     * @param CreateTipo_ClientesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTipo_ClientesAPIRequest $request)
    {
        $input = $request->all();

        $tipoClientes = $this->tipoClientesRepository->create($input);

        return $this->sendResponse(
            $tipoClientes->toArray(),
            __('messages.saved', ['model' => __('models/tipoClientes.singular')])
        );
    }

    /**
     * Display the specified Tipo_Clientes.
     * GET|HEAD /tipoClientes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Tipo_Clientes $tipoClientes */
        $tipoClientes = $this->tipoClientesRepository->find($id);

        if (empty($tipoClientes)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tipoClientes.singular')])
            );
        }

        return $this->sendResponse(
            $tipoClientes->toArray(),
            __('messages.retrieved', ['model' => __('models/tipoClientes.singular')])
        );
    }

    /**
     * Update the specified Tipo_Clientes in storage.
     * PUT/PATCH /tipoClientes/{id}
     *
     * @param int $id
     * @param UpdateTipo_ClientesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipo_ClientesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Tipo_Clientes $tipoClientes */
        $tipoClientes = $this->tipoClientesRepository->find($id);

        if (empty($tipoClientes)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tipoClientes.singular')])
            );
        }

        $tipoClientes = $this->tipoClientesRepository->update($input, $id);

        return $this->sendResponse(
            $tipoClientes->toArray(),
            __('messages.updated', ['model' => __('models/tipoClientes.singular')])
        );
    }

    /**
     * Remove the specified Tipo_Clientes from storage.
     * DELETE /tipoClientes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Tipo_Clientes $tipoClientes */
        $tipoClientes = $this->tipoClientesRepository->find($id);

        if (empty($tipoClientes)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tipoClientes.singular')])
            );
        }

        $tipoClientes->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/tipoClientes.singular')])
        );
    }
}
