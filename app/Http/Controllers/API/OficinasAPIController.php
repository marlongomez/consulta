<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOficinasAPIRequest;
use App\Http\Requests\API\UpdateOficinasAPIRequest;
use App\Models\Oficinas;
use App\Repositories\OficinasRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OficinasController
 * @package App\Http\Controllers\API
 */

class OficinasAPIController extends AppBaseController
{
    /** @var  OficinasRepository */
    private $oficinasRepository;

    public function __construct(OficinasRepository $oficinasRepo)
    {
        $this->oficinasRepository = $oficinasRepo;
    }

    /**
     * Display a listing of the Oficinas.
     * GET|HEAD /oficinas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $oficinas = $this->oficinasRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $oficinas->toArray(),
            __('messages.retrieved', ['model' => __('models/oficinas.plural')])
        );
    }

    /**
     * Store a newly created Oficinas in storage.
     * POST /oficinas
     *
     * @param CreateOficinasAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOficinasAPIRequest $request)
    {
        $input = $request->all();

        $oficinas = $this->oficinasRepository->create($input);

        return $this->sendResponse(
            $oficinas->toArray(),
            __('messages.saved', ['model' => __('models/oficinas.singular')])
        );
    }

    /**
     * Display the specified Oficinas.
     * GET|HEAD /oficinas/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Oficinas $oficinas */
        $oficinas = $this->oficinasRepository->find($id);

        if (empty($oficinas)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/oficinas.singular')])
            );
        }

        return $this->sendResponse(
            $oficinas->toArray(),
            __('messages.retrieved', ['model' => __('models/oficinas.singular')])
        );
    }

    /**
     * Update the specified Oficinas in storage.
     * PUT/PATCH /oficinas/{id}
     *
     * @param int $id
     * @param UpdateOficinasAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOficinasAPIRequest $request)
    {
        $input = $request->all();

        /** @var Oficinas $oficinas */
        $oficinas = $this->oficinasRepository->find($id);

        if (empty($oficinas)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/oficinas.singular')])
            );
        }

        $oficinas = $this->oficinasRepository->update($input, $id);

        return $this->sendResponse(
            $oficinas->toArray(),
            __('messages.updated', ['model' => __('models/oficinas.singular')])
        );
    }

    /**
     * Remove the specified Oficinas from storage.
     * DELETE /oficinas/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Oficinas $oficinas */
        $oficinas = $this->oficinasRepository->find($id);

        if (empty($oficinas)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/oficinas.singular')])
            );
        }

        $oficinas->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/oficinas.singular')])
        );
    }
}
