<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMunicipioAPIRequest;
use App\Http\Requests\API\UpdateMunicipioAPIRequest;
use App\Models\Municipio;
use App\Repositories\MunicipioRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MunicipioController
 * @package App\Http\Controllers\API
 */

class MunicipioAPIController extends AppBaseController
{
    /** @var  MunicipioRepository */
    private $municipioRepository;

    public function __construct(MunicipioRepository $municipioRepo)
    {
        $this->municipioRepository = $municipioRepo;
    }

    /**
     * Display a listing of the Municipio.
     * GET|HEAD /municipios
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $municipios = $this->municipioRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $municipios->toArray(),
            __('messages.retrieved', ['model' => __('models/municipios.plural')])
        );
    }

    /**
     * Store a newly created Municipio in storage.
     * POST /municipios
     *
     * @param CreateMunicipioAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMunicipioAPIRequest $request)
    {
        $input = $request->all();

        $municipio = $this->municipioRepository->create($input);

        return $this->sendResponse(
            $municipio->toArray(),
            __('messages.saved', ['model' => __('models/municipios.singular')])
        );
    }

    /**
     * Display the specified Municipio.
     * GET|HEAD /municipios/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Municipio $municipio */
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/municipios.singular')])
            );
        }

        return $this->sendResponse(
            $municipio->toArray(),
            __('messages.retrieved', ['model' => __('models/municipios.singular')])
        );
    }

    /**
     * Update the specified Municipio in storage.
     * PUT/PATCH /municipios/{id}
     *
     * @param int $id
     * @param UpdateMunicipioAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMunicipioAPIRequest $request)
    {
        $input = $request->all();

        /** @var Municipio $municipio */
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/municipios.singular')])
            );
        }

        $municipio = $this->municipioRepository->update($input, $id);

        return $this->sendResponse(
            $municipio->toArray(),
            __('messages.updated', ['model' => __('models/municipios.singular')])
        );
    }

    /**
     * Remove the specified Municipio from storage.
     * DELETE /municipios/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Municipio $municipio */
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/municipios.singular')])
            );
        }

        $municipio->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/municipios.singular')])
        );
    }
}
