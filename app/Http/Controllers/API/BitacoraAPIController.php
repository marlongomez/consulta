<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBitacoraAPIRequest;
use App\Http\Requests\API\UpdateBitacoraAPIRequest;
use App\Models\Bitacora;
use App\Repositories\BitacoraRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BitacoraController
 * @package App\Http\Controllers\API
 */

class BitacoraAPIController extends AppBaseController
{
    /** @var  BitacoraRepository */
    private $bitacoraRepository;

    public function __construct(BitacoraRepository $bitacoraRepo)
    {
        $this->bitacoraRepository = $bitacoraRepo;
    }

    /**
     * Display a listing of the Bitacora.
     * GET|HEAD /bitacoras
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $bitacoras = $this->bitacoraRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $bitacoras->toArray(),
            __('messages.retrieved', ['model' => __('models/bitacoras.plural')])
        );
    }

    /**
     * Store a newly created Bitacora in storage.
     * POST /bitacoras
     *
     * @param CreateBitacoraAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBitacoraAPIRequest $request)
    {
        $input = $request->all();

        $bitacora = $this->bitacoraRepository->create($input);

        return $this->sendResponse(
            $bitacora->toArray(),
            __('messages.saved', ['model' => __('models/bitacoras.singular')])
        );
    }

    /**
     * Display the specified Bitacora.
     * GET|HEAD /bitacoras/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Bitacora $bitacora */
        $bitacora = $this->bitacoraRepository->find($id);

        if (empty($bitacora)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/bitacoras.singular')])
            );
        }

        return $this->sendResponse(
            $bitacora->toArray(),
            __('messages.retrieved', ['model' => __('models/bitacoras.singular')])
        );
    }

    /**
     * Update the specified Bitacora in storage.
     * PUT/PATCH /bitacoras/{id}
     *
     * @param int $id
     * @param UpdateBitacoraAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBitacoraAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bitacora $bitacora */
        $bitacora = $this->bitacoraRepository->find($id);

        if (empty($bitacora)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/bitacoras.singular')])
            );
        }

        $bitacora = $this->bitacoraRepository->update($input, $id);

        return $this->sendResponse(
            $bitacora->toArray(),
            __('messages.updated', ['model' => __('models/bitacoras.singular')])
        );
    }

    /**
     * Remove the specified Bitacora from storage.
     * DELETE /bitacoras/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Bitacora $bitacora */
        $bitacora = $this->bitacoraRepository->find($id);

        if (empty($bitacora)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/bitacoras.singular')])
            );
        }

        $bitacora->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/bitacoras.singular')])
        );
    }
}
