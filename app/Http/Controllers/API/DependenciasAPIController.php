<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDependenciasAPIRequest;
use App\Http\Requests\API\UpdateDependenciasAPIRequest;
use App\Models\Dependencias;
use App\Repositories\DependenciasRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DependenciasController
 * @package App\Http\Controllers\API
 */

class DependenciasAPIController extends AppBaseController
{
    /** @var  DependenciasRepository */
    private $dependenciasRepository;

    public function __construct(DependenciasRepository $dependenciasRepo)
    {
        $this->dependenciasRepository = $dependenciasRepo;
    }

    /**
     * Display a listing of the Dependencias.
     * GET|HEAD /dependencias
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $dependencias = $this->dependenciasRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $dependencias->toArray(),
            __('messages.retrieved', ['model' => __('models/dependencias.plural')])
        );
    }

    /**
     * Store a newly created Dependencias in storage.
     * POST /dependencias
     *
     * @param CreateDependenciasAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDependenciasAPIRequest $request)
    {
        $input = $request->all();

        $dependencias = $this->dependenciasRepository->create($input);

        return $this->sendResponse(
            $dependencias->toArray(),
            __('messages.saved', ['model' => __('models/dependencias.singular')])
        );
    }

    /**
     * Display the specified Dependencias.
     * GET|HEAD /dependencias/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Dependencias $dependencias */
        $dependencias = $this->dependenciasRepository->find($id);

        if (empty($dependencias)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/dependencias.singular')])
            );
        }

        return $this->sendResponse(
            $dependencias->toArray(),
            __('messages.retrieved', ['model' => __('models/dependencias.singular')])
        );
    }

    /**
     * Update the specified Dependencias in storage.
     * PUT/PATCH /dependencias/{id}
     *
     * @param int $id
     * @param UpdateDependenciasAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDependenciasAPIRequest $request)
    {
        $input = $request->all();

        /** @var Dependencias $dependencias */
        $dependencias = $this->dependenciasRepository->find($id);

        if (empty($dependencias)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/dependencias.singular')])
            );
        }

        $dependencias = $this->dependenciasRepository->update($input, $id);

        return $this->sendResponse(
            $dependencias->toArray(),
            __('messages.updated', ['model' => __('models/dependencias.singular')])
        );
    }

    /**
     * Remove the specified Dependencias from storage.
     * DELETE /dependencias/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Dependencias $dependencias */
        $dependencias = $this->dependenciasRepository->find($id);

        if (empty($dependencias)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/dependencias.singular')])
            );
        }

        $dependencias->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/dependencias.singular')])
        );
    }
}
