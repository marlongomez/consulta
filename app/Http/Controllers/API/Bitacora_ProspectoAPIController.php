<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBitacora_ProspectoAPIRequest;
use App\Http\Requests\API\UpdateBitacora_ProspectoAPIRequest;
use App\Models\Bitacora_Prospecto;
use App\Repositories\Bitacora_ProspectoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Bitacora_ProspectoController
 * @package App\Http\Controllers\API
 */

class Bitacora_ProspectoAPIController extends AppBaseController
{
    /** @var  Bitacora_ProspectoRepository */
    private $bitacoraProspectoRepository;

    public function __construct(Bitacora_ProspectoRepository $bitacoraProspectoRepo)
    {
        $this->bitacoraProspectoRepository = $bitacoraProspectoRepo;
    }

    /**
     * Display a listing of the Bitacora_Prospecto.
     * GET|HEAD /bitacoraProspectos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $bitacoraProspectos = $this->bitacoraProspectoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $bitacoraProspectos->toArray(),
            __('messages.retrieved', ['model' => __('models/bitacoraProspectos.plural')])
        );
    }

    /**
     * Store a newly created Bitacora_Prospecto in storage.
     * POST /bitacoraProspectos
     *
     * @param CreateBitacora_ProspectoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBitacora_ProspectoAPIRequest $request)
    {
        $input = $request->all();

        $bitacoraProspecto = $this->bitacoraProspectoRepository->create($input);

        return $this->sendResponse(
            $bitacoraProspecto->toArray(),
            __('messages.saved', ['model' => __('models/bitacoraProspectos.singular')])
        );
    }

    /**
     * Display the specified Bitacora_Prospecto.
     * GET|HEAD /bitacoraProspectos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Bitacora_Prospecto $bitacoraProspecto */
        $bitacoraProspecto = $this->bitacoraProspectoRepository->find($id);

        if (empty($bitacoraProspecto)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/bitacoraProspectos.singular')])
            );
        }

        return $this->sendResponse(
            $bitacoraProspecto->toArray(),
            __('messages.retrieved', ['model' => __('models/bitacoraProspectos.singular')])
        );
    }

    /**
     * Update the specified Bitacora_Prospecto in storage.
     * PUT/PATCH /bitacoraProspectos/{id}
     *
     * @param int $id
     * @param UpdateBitacora_ProspectoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBitacora_ProspectoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bitacora_Prospecto $bitacoraProspecto */
        $bitacoraProspecto = $this->bitacoraProspectoRepository->find($id);

        if (empty($bitacoraProspecto)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/bitacoraProspectos.singular')])
            );
        }

        $bitacoraProspecto = $this->bitacoraProspectoRepository->update($input, $id);

        return $this->sendResponse(
            $bitacoraProspecto->toArray(),
            __('messages.updated', ['model' => __('models/bitacoraProspectos.singular')])
        );
    }

    /**
     * Remove the specified Bitacora_Prospecto from storage.
     * DELETE /bitacoraProspectos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Bitacora_Prospecto $bitacoraProspecto */
        $bitacoraProspecto = $this->bitacoraProspectoRepository->find($id);

        if (empty($bitacoraProspecto)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/bitacoraProspectos.singular')])
            );
        }

        $bitacoraProspecto->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/bitacoraProspectos.singular')])
        );
    }
}
