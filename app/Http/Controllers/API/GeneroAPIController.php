<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGeneroAPIRequest;
use App\Http\Requests\API\UpdateGeneroAPIRequest;
use App\Models\Genero;
use App\Repositories\GeneroRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class GeneroController
 * @package App\Http\Controllers\API
 */

class GeneroAPIController extends AppBaseController
{
    /** @var  GeneroRepository */
    private $generoRepository;

    public function __construct(GeneroRepository $generoRepo)
    {
        $this->generoRepository = $generoRepo;
    }

    /**
     * Display a listing of the Genero.
     * GET|HEAD /generos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $generos = $this->generoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $generos->toArray(),
            __('messages.retrieved', ['model' => __('models/generos.plural')])
        );
    }

    /**
     * Store a newly created Genero in storage.
     * POST /generos
     *
     * @param CreateGeneroAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateGeneroAPIRequest $request)
    {
        $input = $request->all();

        $genero = $this->generoRepository->create($input);

        return $this->sendResponse(
            $genero->toArray(),
            __('messages.saved', ['model' => __('models/generos.singular')])
        );
    }

    /**
     * Display the specified Genero.
     * GET|HEAD /generos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Genero $genero */
        $genero = $this->generoRepository->find($id);

        if (empty($genero)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/generos.singular')])
            );
        }

        return $this->sendResponse(
            $genero->toArray(),
            __('messages.retrieved', ['model' => __('models/generos.singular')])
        );
    }

    /**
     * Update the specified Genero in storage.
     * PUT/PATCH /generos/{id}
     *
     * @param int $id
     * @param UpdateGeneroAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGeneroAPIRequest $request)
    {
        $input = $request->all();

        /** @var Genero $genero */
        $genero = $this->generoRepository->find($id);

        if (empty($genero)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/generos.singular')])
            );
        }

        $genero = $this->generoRepository->update($input, $id);

        return $this->sendResponse(
            $genero->toArray(),
            __('messages.updated', ['model' => __('models/generos.singular')])
        );
    }

    /**
     * Remove the specified Genero from storage.
     * DELETE /generos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Genero $genero */
        $genero = $this->generoRepository->find($id);

        if (empty($genero)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/generos.singular')])
            );
        }

        $genero->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/generos.singular')])
        );
    }
}
