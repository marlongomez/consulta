<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Flash;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Jenssegers\Agent\Agent;

class ReporteDependenciasController extends Controller
{

    public function show(){

        //dd("entrando a reportedependencias");
        //$ldate = date('Y-m-d');
        //view()->share('ldate',$ldate);

        return view('reportesDependencias.create');
    }

    public function dependenciaPDF(Request $request){

        $id_dependencia = $request->id_dependencia;
        
        $clientes=\DB::table('clientes')
            ->select('clientes.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente','municipio.nombre as municipio', 'localidades.nombre_localidad as localidad')
            ->join('dependencias','dependencias.id_dependencia','=','clientes.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','clientes.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','clientes.id_tipo_cliente')
            ->join('municipio','municipio.id_municipio','=','clientes.id_municipio')
            ->join('localidades','localidades.id_localidad','=','clientes.id_localidad')
            ->where("clientes.id_dependencia", "=", $id_dependencia)
            ->get()
            ->all();

        //dd($clientes);
        
        if (empty($clientes)) {
            # code...
            Flash::error('Busqueda sin datos..');
            return view('reportesDependencias.create');

        }else{

            $dependencia = $clientes[0]->dependencia;

            //view()->share('nombre',$nombre);
            //view()->share('contador',$contador);
            view()->share('dependencia',$dependencia);
            view()->share('clientes',$clientes);

            return \PDF::loadView('reportesDependencias.pdf_dependencia')
                ->setOption('margin-top', '5mm')
                ->setOption('margin-bottom', '15mm')
                ->setPaper('letter', 'landscape')
                ->setOption('footer-right','Página [page] de [topage]')
                ->setOption('footer-font-size','8')
                ->download($dependencia.'.pdf');
        }  
        


    }
    
    
}