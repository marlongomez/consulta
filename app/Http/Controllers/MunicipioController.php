<?php

namespace App\Http\Controllers;

use App\DataTables\MunicipioDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMunicipioRequest;
use App\Http\Requests\UpdateMunicipioRequest;
use App\Repositories\MunicipioRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MunicipioController extends AppBaseController
{
    /** @var  MunicipioRepository */
    private $municipioRepository;

    public function __construct(MunicipioRepository $municipioRepo)
    {
        $this->municipioRepository = $municipioRepo;
    }

    /**
     * Display a listing of the Municipio.
     *
     * @param MunicipioDataTable $municipioDataTable
     * @return Response
     */
    public function index(MunicipioDataTable $municipioDataTable)
    {
        return $municipioDataTable->render('municipios.index');
    }

    /**
     * Show the form for creating a new Municipio.
     *
     * @return Response
     */
    public function create()
    {
        return view('municipios.create');
    }

    /**
     * Store a newly created Municipio in storage.
     *
     * @param CreateMunicipioRequest $request
     *
     * @return Response
     */
    public function store(CreateMunicipioRequest $request)
    {
        $input = $request->all();

        $municipio = $this->municipioRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/municipios.singular')]));

        return redirect(route('municipios.index'));
    }

    /**
     * Display the specified Municipio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            Flash::error(__('messages.not_found', ['model' => __('models/municipios.singular')]));

            return redirect(route('municipios.index'));
        }

        return view('municipios.show')->with('municipio', $municipio);
    }

    /**
     * Show the form for editing the specified Municipio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            Flash::error(__('messages.not_found', ['model' => __('models/municipios.singular')]));

            return redirect(route('municipios.index'));
        }

        return view('municipios.edit')->with('municipio', $municipio);
    }

    /**
     * Update the specified Municipio in storage.
     *
     * @param  int              $id
     * @param UpdateMunicipioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMunicipioRequest $request)
    {
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            Flash::error(__('messages.not_found', ['model' => __('models/municipios.singular')]));

            return redirect(route('municipios.index'));
        }

        $municipio = $this->municipioRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/municipios.singular')]));

        return redirect(route('municipios.index'));
    }

    /**
     * Remove the specified Municipio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            Flash::error(__('messages.not_found', ['model' => __('models/municipios.singular')]));

            return redirect(route('municipios.index'));
        }

        $this->municipioRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/municipios.singular')]));

        return redirect(route('municipios.index'));
    }
}
