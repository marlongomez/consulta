<?php

namespace App\Http\Controllers;

use App\DataTables\Tipo_ClientesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTipo_ClientesRequest;
use App\Http\Requests\UpdateTipo_ClientesRequest;
use App\Repositories\Tipo_ClientesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Tipo_ClientesController extends AppBaseController
{
    /** @var  Tipo_ClientesRepository */
    private $tipoClientesRepository;

    public function __construct(Tipo_ClientesRepository $tipoClientesRepo)
    {
        $this->tipoClientesRepository = $tipoClientesRepo;
    }

    /**
     * Display a listing of the Tipo_Clientes.
     *
     * @param Tipo_ClientesDataTable $tipoClientesDataTable
     * @return Response
     */
    public function index(Tipo_ClientesDataTable $tipoClientesDataTable)
    {
        return $tipoClientesDataTable->render('tipo__clientes.index');
    }

    /**
     * Show the form for creating a new Tipo_Clientes.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo__clientes.create');
    }

    /**
     * Store a newly created Tipo_Clientes in storage.
     *
     * @param CreateTipo_ClientesRequest $request
     *
     * @return Response
     */
    public function store(CreateTipo_ClientesRequest $request)
    {
        $input = $request->all();

        $tipoClientes = $this->tipoClientesRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tipoClientes.singular')]));

        return redirect(route('tipoClientes.index'));
    }

    /**
     * Display the specified Tipo_Clientes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoClientes = $this->tipoClientesRepository->find($id);

        if (empty($tipoClientes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tipoClientes.singular')]));

            return redirect(route('tipoClientes.index'));
        }

        return view('tipo__clientes.show')->with('tipoClientes', $tipoClientes);
    }

    /**
     * Show the form for editing the specified Tipo_Clientes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoClientes = $this->tipoClientesRepository->find($id);

        if (empty($tipoClientes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tipoClientes.singular')]));

            return redirect(route('tipoClientes.index'));
        }

        return view('tipo__clientes.edit')->with('tipoClientes', $tipoClientes);
    }

    /**
     * Update the specified Tipo_Clientes in storage.
     *
     * @param  int              $id
     * @param UpdateTipo_ClientesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipo_ClientesRequest $request)
    {
        $tipoClientes = $this->tipoClientesRepository->find($id);

        if (empty($tipoClientes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tipoClientes.singular')]));

            return redirect(route('tipoClientes.index'));
        }

        $tipoClientes = $this->tipoClientesRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tipoClientes.singular')]));

        return redirect(route('tipoClientes.index'));
    }

    /**
     * Remove the specified Tipo_Clientes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoClientes = $this->tipoClientesRepository->find($id);

        if (empty($tipoClientes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tipoClientes.singular')]));

            return redirect(route('tipoClientes.index'));
        }

        $this->tipoClientesRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tipoClientes.singular')]));

        return redirect(route('tipoClientes.index'));
    }
}
