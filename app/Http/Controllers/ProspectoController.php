<?php

namespace App\Http\Controllers;

use App\DataTables\ProspectoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProspectoRequest;
use App\Http\Requests\UpdateProspectoRequest;
use App\Repositories\ProspectoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Prospecto;

class ProspectoController extends AppBaseController
{
    /** @var  ProspectoRepository */
    private $prospectoRepository;

    public function __construct(ProspectoRepository $prospectoRepo)
    {
        $this->prospectoRepository = $prospectoRepo;
    }

    /**
     * Display a listing of the Prospecto.
     *
     * @param ProspectoDataTable $prospectoDataTable
     * @return Response
     */
    public function index(ProspectoDataTable $prospectoDataTable)
    {
        return $prospectoDataTable->render('prospectos.index');
    }

    /**
     * Show the form for creating a new Prospecto.
     *
     * @return Response
     */
    public function create()
    {
        return view('prospectos.create');
    }

    /**
     * Store a newly created Prospecto in storage.
     *
     * @param CreateProspectoRequest $request
     *
     * @return Response
     */
    public function store(CreateProspectoRequest $request)
    {

        //consultando rfc insertado
        $rfc =$request->rfc_prospecto;

        //buscando si se encuentra dentro del sistema
        $findrfc = Prospecto::where("rfc_prospecto", "=", $rfc)->get()->toArray();

        //dd($findrfc);
        if (empty($findrfc)) {
            $input = $request->all();

            $prospecto = $this->prospectoRepository->create($input);

            Flash::success(__('messages.saved', ['model' => __('models/prospectos.singular')]));

            return redirect(route('prospectos.index'));

        }else{
            //si lo encuentra manda un mensaje de que se encuentra en el sistema
            Flash::error('El cliente prospecto ya se encuentra registrado');
            return redirect(route('prospectos.create'));

        }

    }

    /**
     * Display the specified Prospecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $id = decrypt($id);

        $prospecto=\DB::table('prospecto')
            ->select('prospecto.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente')
            ->join('dependencias','dependencias.id_dependencia','=','prospecto.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','prospecto.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','prospecto.id_tipo_cliente')
            ->where("id_prospecto", "=", $id)
            ->get()
            ->all();

        if (empty($prospecto)) {
            Flash::error(__('messages.not_found', ['model' => __('models/prospectos.singular')]));

            return redirect(route('prospectos.index'));
        }

        return view('prospectos.show')->with('prospecto', $prospecto);
    }

    /**
     * Show the form for editing the specified Prospecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        
        $prospecto = $this->prospectoRepository->find($id);

        if (empty($prospecto)) {
            Flash::error(__('messages.not_found', ['model' => __('models/prospectos.singular')]));

            return redirect(route('prospectos.index'));
        }

        return view('prospectos.edit')->with('prospecto', $prospecto);
    }

    /**
     * Update the specified Prospecto in storage.
     *
     * @param  int              $id
     * @param UpdateProspectoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProspectoRequest $request)
    {
        $prospecto = $this->prospectoRepository->find($id);

        if (empty($prospecto)) {
            Flash::error(__('messages.not_found', ['model' => __('models/prospectos.singular')]));

            return redirect(route('prospectos.index'));
        }

        $prospecto = $this->prospectoRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/prospectos.singular')]));

        return redirect(route('prospectos.index'));
    }

    /**
     * Remove the specified Prospecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prospecto = $this->prospectoRepository->find($id);

        if (empty($prospecto)) {
            Flash::error(__('messages.not_found', ['model' => __('models/prospectos.singular')]));

            return redirect(route('prospectos.index'));
        }

        $this->prospectoRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/prospectos.singular')]));

        return redirect(route('prospectos.index'));
    }
}
