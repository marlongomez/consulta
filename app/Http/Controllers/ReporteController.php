<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Flash;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Jenssegers\Agent\Agent;

class ReporteController extends Controller
{

    public function show(){

        //dd("entrando al controlador reportes a la funcion de clientes");
        //$ldate = date('Y-m-d');
        //view()->share('ldate',$ldate);

        return view('reportesMunicipio.create');
    }

    public function municipioPDF(Request $request){

        $id_municipio = $request->id_municipio;
        
        $clientes=\DB::table('clientes')
            ->select('clientes.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente','municipio.nombre as municipio', 'localidades.nombre_localidad as localidad')
            ->join('dependencias','dependencias.id_dependencia','=','clientes.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','clientes.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','clientes.id_tipo_cliente')
            ->join('municipio','municipio.id_municipio','=','clientes.id_municipio')
            ->join('localidades','localidades.id_localidad','=','clientes.id_localidad')
            ->where("clientes.id_municipio", "=", $id_municipio)
            ->get()
            ->all();
        
        if (empty($clientes)) {
            # code...
            Flash::error('Busqueda sin datos..');
            return view('reportesMunicipio.create');

        }else{

            $municipio = $clientes[0]->municipio;

            //view()->share('nombre',$nombre);
            //view()->share('contador',$contador);
            view()->share('municipio',$municipio);
            view()->share('clientes',$clientes);

            return \PDF::loadView('reportesMunicipio.pdf_municipio')
                ->setOption('margin-top', '5mm')
                ->setOption('margin-bottom', '15mm')
                ->setPaper('letter', 'landscape')
                ->setOption('footer-right','Página [page] de [topage]')
                ->setOption('footer-font-size','8')
                ->download($municipio.'.pdf');
        }  
        


    }
    
    
}