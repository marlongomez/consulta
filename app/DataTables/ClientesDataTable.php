<?php

namespace App\DataTables;

use App\Models\Clientes;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ClientesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'clientes.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Clientes $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Clientes $model)
    {
        return $model->newQuery()->select('clientes.*', 'dependencias.nombre_dependencia as dependencia', 'oficinas.nombre_oficina as oficina', 'tipo_clientes.nombre_tipo as tipo_cliente')
            ->join('dependencias','dependencias.id_dependencia','=','clientes.id_dependencia')
            ->join('oficinas','oficinas.id_oficina','=','clientes.id_oficina')
            ->join('tipo_clientes','tipo_clientes.id_tipo_cliente','=','clientes.id_tipo_cliente');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'nombre_cliente' => new Column(['title' => __('models/clientes.fields.nombre_cliente'), 'data' => 'nombre_cliente']),
            'rfc_cliente' => new Column(['title' => __('models/clientes.fields.rfc_cliente'), 'data' => 'rfc_cliente']),
            'telefono' => new Column(['title' => __('models/clientes.fields.telefono'), 'data' => 'telefono']),
            ['data' => 'dependencia', 'name' => 'dependencias.nombre_dependencia', 'title' => 'Dependencia'],
            ['data' => 'oficina', 'name' => 'oficinas.nombre_oficina', 'title' => 'Oficina'],
            'correo_electronico' => new Column(['title' => __('models/clientes.fields.correo_electronico'), 'data' => 'correo_electronico']),
            ['data' => 'tipo_cliente', 'name' => 'tipo_clientes.nombre_tipo', 'title' => 'Tipo de cliente'],
            ['data' => 'agendado', 'name' => 'clientes.agendado', 'title' => 'Agendado']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'clientes_datatable_' . time();
    }
}
