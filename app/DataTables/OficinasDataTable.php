<?php

namespace App\DataTables;

use App\Models\Oficinas;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class OficinasDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'oficinas.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Oficinas $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Oficinas $model)
    {
        return $model->newQuery()->select('oficinas.*', 'dependencias.nombre_dependencia as dependencia')
            ->join('dependencias','dependencias.id_dependencia','=','oficinas.id_dependencia')
            ;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'nombre_oficina' => new Column(['title' => __('models/oficinas.fields.nombre_oficina'), 'data' => 'nombre_oficina']),
            ['data' => 'dependencia', 'name' => 'dependencias.nombre_dependencia', 'title' => 'Dependencia'],
            'direccion_oficina' => new Column(['title' => __('models/oficinas.fields.direccion_oficina'), 'data' => 'direccion_oficina']),
            'apertura_oficina' => new Column(['title' => __('models/oficinas.fields.apertura_oficina'), 'data' => 'apertura_oficina']),
            'cierre_oficina' => new Column(['title' => __('models/oficinas.fields.cierre_oficina'), 'data' => 'cierre_oficina']),
            'observaciones' => new Column(['title' => __('models/oficinas.fields.observaciones'), 'data' => 'observaciones'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'oficinas_datatable_' . time();
    }
}
