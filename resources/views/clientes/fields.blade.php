<!-- Nombre Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_cliente', __('models/clientes.fields.nombre_cliente').':', ['class' => 'form-group col-sm-6']) !!}
    {!! Form::text('nombre_cliente', null, ['class' => 'form-control col-sm-6']) !!}
</div>

<!-- Rfc Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rfc_cliente', __('models/clientes.fields.rfc_cliente').':', ['class' => 'form-group col-sm-6']) !!}
    {!! Form::text('rfc_cliente', null, ['class' => 'form-control col-sm-6']) !!}
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono', __('models/clientes.fields.telefono').':', ['class' => 'form-group col-sm-6']) !!}
    {!! Form::text('telefono', null, ['class' => 'form-control col-sm-6']) !!}
</div>

<!-- Correo Electronico Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correo_electronico', __('models/clientes.fields.correo_electronico').':', ['class' => 'form-group col-sm-6']) !!}
    {!! Form::text('correo_electronico', null, ['class' => 'form-control col-sm-6']) !!}
</div>

<!-- Municipio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_municipio', "Municipio:", ['class' => 'form-group col-sm-6']) !!}
    {!! Form::select("id_municipio",\App\Models\Municipio::pluck('nombre','id_municipio'),null,['class'=>'form-control col-sm-6 js-example-basic-single']) !!}
</div>

<!-- Localidad Field  -->
<div class="form-group col-sm-6">
    {!! Form::label("id_localidad","Localidad:", ['class' => 'form-group col-sm-6']) !!}
    {!! Form::select("id_localidad",\App\Models\Localidades::pluck('nombre_localidad','id_localidad'),null,['class'=>'form-control col-sm-6 js-example-basic-single']) !!}
</div>

<!-- Id Dependencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_dependencia', __('models/clientes.fields.id_dependencia').':', ['class' => 'form-group col-sm-6']) !!}
    {!! Form::select("id_dependencia",\App\Models\Dependencias::pluck('nombre_dependencia','id_dependencia'),null,['class'=>'form-control col-sm-6 js-example-basic-single']) !!}
</div>

<!-- Id Oficina Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_oficina', __('models/clientes.fields.id_oficina').':', ['class' => 'form-group col-sm-6']) !!}
    {!! Form::select("id_oficina",\App\Models\Oficinas::pluck('nombre_oficina','id_oficina'),null,['class'=>'form-control col-sm-6 js-example-basic-single']) !!}
</div>

<!-- Id Tipo Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_tipo_cliente', __('models/clientes.fields.id_tipo_cliente').':', ['class' => 'form-group col-sm-6']) !!}
    {!! Form::select("id_tipo_cliente",\App\Models\Tipo_Clientes::pluck('nombre_tipo','id_tipo_cliente'),null,['class'=>'form-control col-sm-6 js-example-basic-single']) !!}
</div>

<!-- Agendado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('agendado', 'Agendado (AñoMes):', ['class' => 'form-group col-sm-6']) !!}
    {!! Form::text('agendado', null, ['class' => 'form-control col-sm-6']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('clientes.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>

@push('scripts')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
      });
    </script>


    <script>
        $('#id_municipio').on('change', function () {
            getLocalidades($(this).val());
        });
        function getLocalidades(id_mun) {
            $.get("{{url('localidades/get-localidades')}}/" + id_mun, function (data) {
                $("#id_localidad").html(data);
            });
        }
        $(document).ready(function () {
            getLocalidades($('#id_municipio').val());
        });
    </script>
@endpush
