{!! Form::open(['route' => ['clientes.destroy', $id_cliente], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{!! route('bitacoras.create', Crypt::encrypt($id_cliente)) !!}" class='btn btn-success btn-xs' title="Agregar Observacion.">
        <i class="glyphicon glyphicon-plus"></i>
    </a>
    <a href="{{ route('clientes.show', Crypt::encrypt($id_cliente)) }}" class='btn btn-primary btn-xs' title="Mostrar datos del cliente">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('clientes.edit', Crypt::encrypt($id_cliente)) }}" class='btn btn-warning btn-xs' title="Editar el cliente">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    <a href="{{ route('bitacoras.index', Crypt::encrypt($id_cliente)) }}" class='btn btn-info btn-xs' title="Listar bitacora">
        <i class="glyphicon glyphicon-th-list"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
