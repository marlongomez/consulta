@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/localidades.singular')
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($localidades, ['route' => ['localidades.update', $localidades->id_localidad], 'method' => 'patch']) !!}

                        @include('localidades.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
