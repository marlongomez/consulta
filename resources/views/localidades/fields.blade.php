<!-- Id Municipio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_municipio', __('models/localidades.fields.id_municipio').':') !!}
    {!! Form::select('id_municipio', \App\Models\Municipio::pluck('nombre','id_municipio'), null, ['class' => 'form-control js-example-basic-single']) !!}
</div>

<!-- Nombre Localidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_localidad', __('models/localidades.fields.nombre_localidad').':') !!}
    {!! Form::text('nombre_localidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('localidades.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
