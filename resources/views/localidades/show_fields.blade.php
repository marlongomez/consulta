<!-- Id Municipio Field -->
<div class="form-group">
    {!! Form::label('id_municipio', __('models/localidades.fields.id_municipio').':') !!}
    <p>{{ $localidades->id_municipio }}</p>
</div>

<!-- Nombre Localidad Field -->
<div class="form-group">
    {!! Form::label('nombre_localidad', __('models/localidades.fields.nombre_localidad').':') !!}
    <p>{{ $localidades->nombre_localidad }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/localidades.fields.created_at').':') !!}
    <p>{{ $localidades->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/localidades.fields.updated_at').':') !!}
    <p>{{ $localidades->updated_at }}</p>
</div>

