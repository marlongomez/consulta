<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_municipio', 'Municipio:') !!}
    {!! Form::select('id_municipio', \App\Models\Municipio::pluck('nombre','id_municipio'), null, ['class' => 'form-control js-example-basic-single']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('PDF', ['class' => 'btn btn-default fa fa-file-pdf-o']) !!}
</div>
