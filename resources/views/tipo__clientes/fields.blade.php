<!-- Nombre Tipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_tipo', __('models/tipoClientes.fields.nombre_tipo').':') !!}
    {!! Form::text('nombre_tipo', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('tipoClientes.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
