{!! Form::open(['route' => ['tipoClientes.destroy', $id_tipo_cliente], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('tipoClientes.show', $id_tipo_cliente) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('tipoClientes.edit', $id_tipo_cliente) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
