<!-- Nombre Tipo Field -->
<div class="form-group">
    {!! Form::label('nombre_tipo', __('models/tipoClientes.fields.nombre_tipo').':') !!}
    <p>{{ $tipoClientes->nombre_tipo }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/tipoClientes.fields.created_at').':') !!}
    <p>{{ $tipoClientes->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/tipoClientes.fields.updated_at').':') !!}
    <p>{{ $tipoClientes->updated_at }}</p>
</div>

