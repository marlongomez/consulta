@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/tipoClientes.singular')
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tipoClientes, ['route' => ['tipoClientes.update', $tipoClientes->id_tipo_cliente], 'method' => 'patch']) !!}

                        @include('tipo__clientes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
