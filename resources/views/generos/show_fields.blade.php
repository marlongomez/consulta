<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', __('models/generos.fields.nombre').':') !!}
    <p>{{ $genero->nombre }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/generos.fields.created_at').':') !!}
    <p>{{ $genero->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/generos.fields.updated_at').':') !!}
    <p>{{ $genero->updated_at }}</p>
</div>

