<!DOCTYPE html>
<html>
<head>
    <title>Reporte de {{$dependencia}} </title>
    <meta charset="UTF-8">
    <meta name=description content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
</head>
<header class="cabecera">

</header>

<body>
    <div class="generales">
        <div class="empleado">
                <p style="margin-left: 170px ;text-align: center;width: 750px; font-size: 20px;">CONTROL ESTADISTICOS DE LOS CLIENTES DE LA DEPENDENCIA: <strong>{{$dependencia}}</strong> </p>
                
        </div>  
    </div>
    
    <div class="container">
        <div class="contenido">
            <table class="table table-bordered" style="text-align: justify-all;position: relative;text-align: justify-all;">
                <thead style="background-color: rgb(38,202,211);">
                    <th>Nombre del Cliente</th>
                    <th>RFC</th>
                    <th>Dependencia</th>
                    <th>Oficina</th>
                    <th>Municipio</th>
                    <th>Localidad</th>
                    <th>Tipo de cliente</th>
                    <th>Agendado</th>
                </thead>
                <tbody>
                    @foreach ($clientes as $value)
                        <tr>
                            <td>{{ $value->nombre_cliente }}</td>
                            <td>{{ $value->rfc_cliente }}</td>
                            <td>{{ $value->dependencia }}</td>
                            <td>{{ $value->oficina }}</td>
                            <td>{{ $value->municipio }}</td>
                            <td>{{ $value->localidad }}</td>
                            <td>{{ $value->tipo_cliente }}</td>
                            <td>{{ $value->agendado }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
        </div>
            
    </div>
    
</body>
</html>