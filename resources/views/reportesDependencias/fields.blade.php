<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_dependencia', __('models/oficinas.fields.id_dependencia').':') !!}
    {!! Form::select('id_dependencia',  \App\Models\Dependencias::pluck('nombre_dependencia','id_dependencia'), null, ['class' => 'form-control col-sm-6 js-example-basic-single']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('PDF', ['class' => 'btn btn-default fa fa-file-pdf-o']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
      });
    </script>
@endpush