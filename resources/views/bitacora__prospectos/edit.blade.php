@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/bitacoraProspectos.singular')
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($bitacoraProspecto, ['route' => ['bitacoraProspectos.update', $bitacoraProspecto->id_bitacora_prospecto], 'method' => 'patch']) !!}

                        @include('bitacora__prospectos.fieldsedit')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
