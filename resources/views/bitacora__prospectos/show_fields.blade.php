<!-- Id Prospecto Field -->
<div class="form-group">
    {!! Form::label('id_prospecto', __('models/bitacoraProspectos.fields.id_prospecto').':') !!}
    <p>{{ $bitacoraProspecto->id_prospecto }}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', __('models/bitacoraProspectos.fields.fecha').':') !!}
    <p>{{ $bitacoraProspecto->fecha }}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', __('models/bitacoraProspectos.fields.observaciones').':') !!}
    <p>{{ $bitacoraProspecto->observaciones }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/bitacoraProspectos.fields.created_at').':') !!}
    <p>{{ $bitacoraProspecto->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/bitacoraProspectos.fields.updated_at').':') !!}
    <p>{{ $bitacoraProspecto->updated_at }}</p>
</div>

