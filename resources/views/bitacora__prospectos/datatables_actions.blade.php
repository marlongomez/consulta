{!! Form::open(['route' => ['bitacoraProspectos.destroy', $id_bitacora_prospecto], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('bitacoraProspectos.edit', $id_bitacora_prospecto) }}" class='btn btn-warning btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
