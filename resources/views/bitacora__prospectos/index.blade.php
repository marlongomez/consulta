@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            @lang('models/bitacoraProspectos.singular')
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div>
                    @foreach ($prospecto as $pros)
                        <!-- Nombre Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('nombre_prospecto','Nombre cliente prospecto:') !!}
                            <p>{{ $pros->nombre_prospecto }}</p>
                        </div>

                        <!-- Rfc Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('rfc_prospecto', 'RFC cliente prospecto:') !!}
                            <p>{{ $pros->rfc_prospecto }}</p>
                        </div>

                        <!-- Telefono Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('telefono', __('models/clientes.fields.telefono').':') !!}
                            <p>{{ $pros->telefono }}</p>
                        </div>

                        <!-- Id Dependencia Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_dependencia', __('models/clientes.fields.id_dependencia').':') !!}
                            <p>{{ $pros->dependencia }}</p>
                        </div>

                        <!-- Id Oficina Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_oficina', __('models/clientes.fields.id_oficina').':') !!}
                            <p>{{ $pros->oficina }}</p>
                        </div>

                        <!-- Correo Electronico Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('correo_electronico', __('models/clientes.fields.correo_electronico').':') !!}
                            <p>{{ $pros->correo_electronico }}</p>
                        </div>

                        <!-- Id Tipo Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_tipo_cliente', __('models/clientes.fields.id_tipo_cliente').':') !!}
                            <p>{{ $pros->tipo_cliente }}</p>
                        </div>

                        <!-- Id Tipo Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('created_at', 'Creado el:') !!}
                            <p>{{ $pros->created_at }}</p>
                        </div>

                        <a class="btn btn-default btn-sm no-corner" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('bitacoraProspectos.create', Crypt::encrypt($pros->id_prospecto)) !!}"><i class="glyphicon glyphicon-plus"></i>Agregar Observación</a>
                    @endforeach

                </div>
                    @include('bitacora__prospectos.table')

                    <a href="{!! route('prospectos.index') !!}" class="btn btn-primary">Regresar</a>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

