@foreach ($prospecto as $pros)

    <!-- Id Prospecto Field -->
        {!! Form::hidden('id_prospecto', $pros->id_prospecto, ['class' => 'form-control']) !!}


    <!-- Fecha Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('fecha', __('models/bitacoraProspectos.fields.fecha').':') !!}
        {!! Form::date('fecha', null, ['class' => 'form-control','id'=>'fecha']) !!}
    </div>

    <!-- Observaciones Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('observaciones', __('models/bitacoraProspectos.fields.observaciones').':') !!}
        {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('bitacoraProspectos.index', Crypt::encrypt($pros->id_prospecto)) }}" class="btn btn-default">@lang('crud.cancel')</a>
    </div>

@endforeach
