@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/bitacoraProspectos.singular')
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <fieldset>
                        <legend>Datos del solicitante:</legend>

                        @foreach ($prospecto as $pros)
                            <!-- Nombre Cliente Field -->
                            <div class="form-group col-sm-6">
                            {!! Form::label('nombre_prospecto','Nombre cliente prospecto:') !!}
                            <p>{{ $pros->nombre_prospecto }}</p>
                        </div>

                        <!-- Rfc Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('rfc_prospecto', 'RFC cliente prospecto:') !!}
                            <p>{{ $pros->rfc_prospecto }}</p>
                        </div>

                        <!-- Telefono Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('telefono', __('models/clientes.fields.telefono').':') !!}
                            <p>{{ $pros->telefono }}</p>
                        </div>

                        <!-- Id Dependencia Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_dependencia', __('models/clientes.fields.id_dependencia').':') !!}
                            <p>{{ $pros->dependencia }}</p>
                        </div>

                        <!-- Id Oficina Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_oficina', __('models/clientes.fields.id_oficina').':') !!}
                            <p>{{ $pros->oficina }}</p>
                        </div>

                        <!-- Correo Electronico Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('correo_electronico', __('models/clientes.fields.correo_electronico').':') !!}
                            <p>{{ $pros->correo_electronico }}</p>
                        </div>

                        <!-- Id Tipo Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_tipo_cliente', __('models/clientes.fields.id_tipo_cliente').':') !!}
                            <p>{{ $pros->tipo_cliente }}</p>
                        </div>

                        <!-- Id Tipo Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('created_at', 'Creado el:') !!}
                            <p>{{ $pros->created_at }}</p>
                        </div>

                        @endforeach
                    </fieldset>
                    
                <div class="row">
                    {!! Form::open(['route' => 'bitacoraProspectos.store']) !!}

                        @include('bitacora__prospectos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
