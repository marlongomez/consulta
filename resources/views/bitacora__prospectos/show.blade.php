@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/bitacoraProspectos.singular')
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('bitacora__prospectos.show_fields')
                    <a href="{{ route('bitacoraProspectos.index') }}" class="btn btn-default">
                        @lang('crud.back')
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
