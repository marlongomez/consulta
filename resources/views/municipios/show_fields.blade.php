<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', __('models/municipios.fields.nombre').':') !!}
    <p>{{ $municipio->nombre }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/municipios.fields.created_at').':') !!}
    <p>{{ $municipio->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/municipios.fields.updated_at').':') !!}
    <p>{{ $municipio->updated_at }}</p>
</div>

