{!! Form::open(['route' => ['municipios.destroy', $id_municipio], 'method' => 'delete']) !!}
<div class='btn-group'>
    <!--<a href="{{ route('municipios.show', $id_municipio) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>-->
    <a href="{{ route('municipios.edit', $id_municipio) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
