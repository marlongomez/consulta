<aside class="main-sidebar" id="sidebar-wrapper">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('public/images/icons/user.jpg')}}" class="img-circle"
                     alt="User Image"/>
            </div>
            <div class="pull-left info">
                @if (Auth::guest())
                <p>InfyOm</p>
                @else
                    <p>{{ Auth::user()->name}}</p>
                @endif
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i>@lang('auth.app.online')</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="@lang('auth.app.search')..."/>
          <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
            </div>
        </form>
        <!-- Sidebar Menu -->
        <li class="">
                    <a href="#homeSubmenuRedes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="color: white">
                         <img src="{{asset('public/images/icons/Catalogo.png')}}"  alt="" width="35" height="35" class="img-circle"/>
                            Catalogos
                    </a>
                    <ul class="collapse list-group" id="homeSubmenuRedes">
                        <ul style="list-style:circle;color: white;" class="opciones">
                            @include('layouts.menu')
                        </ul>
                    </ul>
        </li>
        <li class="{{ Request::is('clientes*') ? 'active' : '' }}">
            <a href="{{ route('clientes.index') }}"><img src="{{asset('public/images/icons/Cliente.png')}}"  alt="" width="35" height="35" class="img-circle"/><span>@lang('models/clientes.plural')</span></a>
        </li>
        <li class="{{ Request::is('prospectos*') ? 'active' : '' }}">
            <a href="{{ route('prospectos.index') }}"><img src="{{asset('public/images/icons/Cliente.png')}}"  alt="" width="35" height="35" class="img-circle"/></i><span>@lang('models/prospectos.plural')</span></a>
        </li>
        <li class="">
            <a href="#homeSubmenuReportes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="color: white">
                         <img src="{{asset('public/images/icons/Reporte.jpg')}}"  alt="" width="35" height="35" class="img-circle"/>
                            Reportes
                    </a>
                    <ul class="collapse list-group" id="homeSubmenuReportes">
                        <ul style="list-style:circle;color: white;" class="opciones">
                            @include('layouts.reportes')
                        </ul>
                    </ul>
            
        </li>
        <!--<ul class="sidebar-menu" data-widget="tree">
            @include('layouts.menu')
        </ul>-->
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>