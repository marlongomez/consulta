<li class="{{ Request::is('municipios*') ? 'active' : '' }}">
    <a href="{{ route('municipios.index') }}"><i class="fa fa-map-marker"></i><span>@lang('models/municipios.plural')</span></a>
</li>

<li class="{{ Request::is('localidades*') ? 'active' : '' }}">
    <a href="{{ route('localidades.index') }}"><i class="fa fa-map-marker"></i><span>@lang('models/localidades.plural')</span></a>
</li>

<li class="{{ Request::is('dependencias*') ? 'active' : '' }}">
    <a href="{{ route('dependencias.index') }}"><i class="fa fa-building"></i><span>@lang('models/dependencias.plural')</span></a>
</li>

<li class="{{ Request::is('oficinas*') ? 'active' : '' }}">
    <a href="{{ route('oficinas.index') }}"><i class="fa fa-briefcase"></i><span>@lang('models/oficinas.plural')</span></a>
</li>

<li class="{{ Request::is('tipoClientes*') ? 'active' : '' }}">
    <a href="{{ route('tipoClientes.index') }}"><i class="fa fa-edit"></i><span>@lang('models/tipoClientes.plural')</span></a>
</li>

