{!! Form::open(['route' => ['oficinas.destroy', $id_oficina], 'method' => 'delete']) !!}
<div class='btn-group'>
    <!--<a href="{{ route('oficinas.show', $id_oficina) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>-->
    <a href="{{ route('oficinas.edit', $id_oficina) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
