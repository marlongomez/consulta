@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/oficinas.singular')
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'oficinas.store']) !!}

                        @include('oficinas.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
      });
    </script>


    <script>
        $('#id_municipio').on('change', function () {
            getLocalidades($(this).val());
        });
        function getLocalidades(id_mun) {
            $.get("{{url('localidades/get-localidades')}}/" + id_mun, function (data) {
                $("#id_localidad").html(data);
            });
        }
        $(document).ready(function () {
            getLocalidades($('#id_municipio').val());
        });
    </script>

@endsection
