<!-- Id Dependencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_dependencia', __('models/oficinas.fields.id_dependencia').':') !!}
    {!! Form::select('id_dependencia',  \App\Models\Dependencias::pluck('nombre_dependencia','id_dependencia'), null, ['class' => 'form-control col-sm-6 js-example-basic-single']) !!}
</div>

<!-- Nombre Oficina Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_oficina', __('models/oficinas.fields.nombre_oficina').':') !!}
    {!! Form::text('nombre_oficina', null, ['class' => 'form-control']) !!}
</div>

<!-- Municipio Field
<div class="form-group col-sm-6">
    {!! Form::label('id_municipio', "Municipio:") !!}
    {!! Form::select("id_municipio",\App\Models\Municipio::pluck('nombre','id_municipio'),null,['class'=>'form-control col-sm-6 js-example-basic-single']) !!}
</div>

   Localidad Field 
<div class="form-group col-sm-6">
    {!! Form::label("id_localidad","Localidad:") !!}
    {!! Form::select("id_localidad",[],null,['class'=>'form-control col-sm-6 js-example-basic-single']) !!}
</div>
-->
<!-- Direccion Oficina Field -->
<div class="form-group col-sm-6">
    {!! Form::label('direccion_oficina', __('models/oficinas.fields.direccion_oficina').':') !!}
    {!! Form::text('direccion_oficina', null, ['class' => 'form-control']) !!}
</div>

<!-- Apertura Oficina Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apertura_oficina', __('models/oficinas.fields.apertura_oficina').':') !!}
    {!! Form::text('apertura_oficina', null, ['class' => 'form-control']) !!}
</div>

<!-- Cierre Oficina Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cierre_oficina', __('models/oficinas.fields.cierre_oficina').':') !!}
    {!! Form::text('cierre_oficina', null, ['class' => 'form-control']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observaciones', __('models/oficinas.fields.observaciones').':') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('oficinas.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>

@push('scripts')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
      });
    </script>
@endpush