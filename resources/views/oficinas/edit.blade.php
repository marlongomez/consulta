@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/oficinas.singular')
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($oficinas, ['route' => ['oficinas.update', $oficinas->id_oficina], 'method' => 'patch']) !!}

                        @include('oficinas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
