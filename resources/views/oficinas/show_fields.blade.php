<!-- Id Dependencia Field -->
<div class="form-group">
    {!! Form::label('id_dependencia', __('models/oficinas.fields.id_dependencia').':') !!}
    <p>{{ $oficinas->id_dependencia }}</p>
</div>

<!-- Nombre Oficina Field -->
<div class="form-group">
    {!! Form::label('nombre_oficina', __('models/oficinas.fields.nombre_oficina').':') !!}
    <p>{{ $oficinas->nombre_oficina }}</p>
</div>

<!-- Direccion Oficina Field -->
<div class="form-group">
    {!! Form::label('direccion_oficina', __('models/oficinas.fields.direccion_oficina').':') !!}
    <p>{{ $oficinas->direccion_oficina }}</p>
</div>

<!-- Apertura Oficina Field -->
<div class="form-group">
    {!! Form::label('apertura_oficina', __('models/oficinas.fields.apertura_oficina').':') !!}
    <p>{{ $oficinas->apertura_oficina }}</p>
</div>

<!-- Cierre Oficina Field -->
<div class="form-group">
    {!! Form::label('cierre_oficina', __('models/oficinas.fields.cierre_oficina').':') !!}
    <p>{{ $oficinas->cierre_oficina }}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', __('models/oficinas.fields.observaciones').':') !!}
    <p>{{ $oficinas->observaciones }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/oficinas.fields.created_at').':') !!}
    <p>{{ $oficinas->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/oficinas.fields.updated_at').':') !!}
    <p>{{ $oficinas->updated_at }}</p>
</div>

