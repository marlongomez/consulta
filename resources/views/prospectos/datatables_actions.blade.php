{!! Form::open(['route' => ['prospectos.destroy', $id_prospecto], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{!! route('bitacoraProspectos.create', Crypt::encrypt($id_prospecto)) !!}" class='btn btn-success btn-xs' title="Agregar Observación.">
        <i class="glyphicon glyphicon-plus"></i>
    </a>
    <a href="{{ route('prospectos.show', Crypt::encrypt($id_prospecto)) }}" class='btn btn-primary btn-xs' title="Mostrar datos del cliente">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('prospectos.edit', Crypt::encrypt($id_prospecto)) }}" class='btn btn-warning btn-xs' title="Editar el cliente">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    <a href="{{ route('bitacoraProspectos.index', Crypt::encrypt($id_prospecto)) }}" class='btn btn-info btn-xs' title="Listar bitacora del cliente prospecto">
        <i class="glyphicon glyphicon-th-list"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
