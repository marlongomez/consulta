@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/prospectos.singular')
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($prospecto, ['route' => ['prospectos.update', $prospecto->id_prospecto], 'method' => 'patch']) !!}

                        @include('prospectos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
