@foreach ($prospecto as $pros)
<!-- Nombre Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_cliente', __('models/prospectos.fields.nombre_prospecto').':') !!}
    <p>{{ $pros->nombre_prospecto }}</p>
</div>

<!-- Rfc Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rfc_cliente', __('models/prospectos.fields.rfc_prospecto').':') !!}
    <p>{{ $pros->rfc_prospecto }}</p>
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono', __('models/clientes.fields.telefono').':') !!}
    <p>{{ $pros->telefono }}</p>
</div>

<!-- Id Dependencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_dependencia', __('models/clientes.fields.id_dependencia').':') !!}
    <p>{{ $pros->dependencia }}</p>
</div>

<!-- Id Oficina Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_oficina', __('models/clientes.fields.id_oficina').':') !!}
    <p>{{ $pros->oficina }}</p>
</div>

<!-- Correo Electronico Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correo_electronico', __('models/clientes.fields.correo_electronico').':') !!}
    <p>{{ $pros->correo_electronico }}</p>
</div>

<!-- Id Tipo Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_tipo_cliente', __('models/clientes.fields.id_tipo_cliente').':') !!}
    <p>{{ $pros->tipo_cliente }}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', __('models/clientes.fields.created_at').':') !!}
    <p>{{ $pros->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', __('models/clientes.fields.updated_at').':') !!}
    <p>{{ $pros->updated_at }}</p>
</div>

<!-- Agendado Field -->
<div class="form-group col-sm-6">
    
</div>


@endforeach
