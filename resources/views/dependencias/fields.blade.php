<!-- Nombre Dependencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_dependencia', __('models/dependencias.fields.nombre_dependencia').':') !!}
    {!! Form::text('nombre_dependencia', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('dependencias.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
