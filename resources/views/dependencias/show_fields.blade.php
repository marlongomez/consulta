<!-- Nombre Dependencia Field -->
<div class="form-group">
    {!! Form::label('nombre_dependencia', __('models/dependencias.fields.nombre_dependencia').':') !!}
    <p>{{ $dependencias->nombre_dependencia }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/dependencias.fields.created_at').':') !!}
    <p>{{ $dependencias->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/dependencias.fields.updated_at').':') !!}
    <p>{{ $dependencias->updated_at }}</p>
</div>

