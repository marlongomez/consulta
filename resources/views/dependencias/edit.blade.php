@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/dependencias.singular')
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($dependencias, ['route' => ['dependencias.update', $dependencias->id_dependencia], 'method' => 'patch']) !!}

                        @include('dependencias.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
