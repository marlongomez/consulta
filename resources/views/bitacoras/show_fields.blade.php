<!-- Id Cliente Field -->
<div class="form-group">
    {!! Form::label('id_cliente', __('models/bitacoras.fields.id_cliente').':') !!}
    <p>{{ $bitacora->id_cliente }}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', __('models/bitacoras.fields.fecha').':') !!}
    <p>{{ $bitacora->fecha }}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', __('models/bitacoras.fields.observaciones').':') !!}
    <p>{{ $bitacora->observaciones }}</p>
</div>

<!-- Ultima Venta Field -->
<div class="form-group">
    {!! Form::label('ultima_venta', __('models/bitacoras.fields.ultima_venta').':') !!}
    <p>{{ $bitacora->ultima_venta }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/bitacoras.fields.created_at').':') !!}
    <p>{{ $bitacora->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/bitacoras.fields.updated_at').':') !!}
    <p>{{ $bitacora->updated_at }}</p>
</div>

