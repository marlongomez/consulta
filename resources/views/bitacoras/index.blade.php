@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            Bitacora del cliente
        </h1>
        <h1 class="pull-right">
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div>
                    @foreach ($clientes as $cliente)
                        <!-- Nombre Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('nombre_cliente', __('models/clientes.fields.nombre_cliente').':') !!}
                            <p>{{ $cliente->nombre_cliente }}</p>
                        </div>

                        <!-- Rfc Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('rfc_cliente', __('models/clientes.fields.rfc_cliente').':') !!}
                            <p>{{ $cliente->rfc_cliente }}</p>
                        </div>

                        <!-- Telefono Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('telefono', __('models/clientes.fields.telefono').':') !!}
                            <p>{{ $cliente->telefono }}</p>
                        </div>

                        <!-- Id Dependencia Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_dependencia', __('models/clientes.fields.id_dependencia').':') !!}
                            <p>{{ $cliente->dependencia }}</p>
                        </div>

                        <!-- Id Oficina Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_oficina', __('models/clientes.fields.id_oficina').':') !!}
                            <p>{{ $cliente->oficina }}</p>
                        </div>

                        <!-- Correo Electronico Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('correo_electronico', __('models/clientes.fields.correo_electronico').':') !!}
                            <p>{{ $cliente->correo_electronico }}</p>
                        </div>

                        <!-- Municipio Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('municipio', 'Municipio:') !!}
                            <p>{{ $cliente->municipio }}</p>
                        </div>

                        <!-- Localidad Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('localidad', 'Localidad:') !!}
                            <p>{{ $cliente->localidad }}</p>
                        </div>

                        <!-- Id Tipo Cliente Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_tipo_cliente', __('models/clientes.fields.id_tipo_cliente').':') !!}
                            <p>{{ $cliente->tipo_cliente }}</p>
                        </div>

                        <!-- Agendado Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('agendado', 'Agendado:') !!}
                            <p>{{ $cliente->agendado }}</p>
                        </div>

                        <a class="btn btn-default btn-sm no-corner" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('bitacoras.create', Crypt::encrypt($cliente->id_cliente)) !!}"><i class="glyphicon glyphicon-plus"></i>Agregar Observación</a>
                    @endforeach



                </div>
                    @include('bitacoras.table')

                    <a href="{!! route('clientes.index') !!}" class="btn btn-primary">Regresar</a>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

