{!! Form::open(['route' => ['bitacoras.destroy', $id_bitacora], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('bitacoras.edit', $id_bitacora) }}" class='btn btn-warning btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
