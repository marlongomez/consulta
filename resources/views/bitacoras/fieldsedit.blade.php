@foreach ($clientes as $cliente)

<!-- Id Cliente Field -->
    {!! Form::hidden('id_cliente', $cliente->id_cliente, ['class' => 'form-control']) !!}
  
<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', __('models/bitacoras.fields.fecha').':') !!}
    {!! Form::date('fecha', null, ['class' => 'form-control','id'=>'fecha']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('observaciones', __('models/bitacoras.fields.observaciones').':') !!}
    {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Ultima Venta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ultima_venta', 'Ultima venta (QuincenasAño):') !!}
    {!! Form::text('ultima_venta', null, ['class' => 'form-control']) !!}
</div>

<!-- Agendado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('agendado', 'Agendado (AñoQuincena):') !!}
    {!! Form::text('agendado', $cliente->agendado, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('bitacoras.index', Crypt::encrypt($cliente->id_cliente)) }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
@endforeach