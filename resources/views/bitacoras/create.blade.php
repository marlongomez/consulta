@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/bitacoras.singular')
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <fieldset>
                        <legend>Datos del solicitante:</legend>

                        @foreach ($clientes as $cliente)
                            <!-- Nombre Cliente Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('nombre_cliente', __('models/clientes.fields.nombre_cliente').':') !!}
                                <p>{{ $cliente->nombre_cliente }}</p>
                            </div>

                            <!-- Rfc Cliente Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('rfc_cliente', __('models/clientes.fields.rfc_cliente').':') !!}
                                <p>{{ $cliente->rfc_cliente }}</p>
                            </div>

                            <!-- Telefono Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('telefono', __('models/clientes.fields.telefono').':') !!}
                                <p>{{ $cliente->telefono }}</p>
                            </div>

                            <!-- Id Dependencia Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('id_dependencia', __('models/clientes.fields.id_dependencia').':') !!}
                                <p>{{ $cliente->dependencia }}</p>
                            </div>

                            <!-- Id Oficina Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('id_oficina', __('models/clientes.fields.id_oficina').':') !!}
                                <p>{{ $cliente->oficina }}</p>
                            </div>

                            <!-- Correo Electronico Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('correo_electronico', __('models/clientes.fields.correo_electronico').':') !!}
                                <p>{{ $cliente->correo_electronico }}</p>
                            </div>

                            <!-- Id Tipo Cliente Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('id_tipo_cliente', __('models/clientes.fields.id_tipo_cliente').':') !!}
                                <p>{{ $cliente->tipo_cliente }}</p>
                            </div>

                            <!-- Agendado Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('agendado', 'Agendado:') !!}
                                <p>{{ $cliente->agendado }}</p>
                            </div>

                        @endforeach
                    </fieldset>
                <div class="row">
                    {!! Form::open(['route' => 'bitacoras.store']) !!}

                        @include('bitacoras.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
