@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('models/bitacoras.singular')
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($bitacora, ['route' => ['bitacoras.update', $bitacora->id_bitacora], 'method' => 'patch']) !!}

                        @include('bitacoras.fieldsedit')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
