<?php

return array (
  'singular' => 'Clientes',
  'plural' => 'Clientes',
  'fields' => 
  array (
    'id_cliente' => 'Id Cliente',
    'nombre_cliente' => 'Nombre Cliente',
    'rfc_cliente' => 'Rfc Cliente',
    'telefono' => 'Telefono',
    'id_dependencia' => 'Id Dependencia',
    'id_oficina' => 'Id Oficina',
    'correo_electronico' => 'Correo Electronico',
    'id_tipo_cliente' => 'Id Tipo Cliente',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
