<?php

return array (
  'singular' => 'Oficinas',
  'plural' => 'Oficinas',
  'fields' => 
  array (
    'id_oficina' => 'Id Oficina',
    'id_dependencia' => 'Id Dependencia',
    'nombre_oficina' => 'Nombre Oficina',
    'direccion_oficina' => 'Direccion Oficina',
    'apertura_oficina' => 'Apertura Oficina',
    'cierre_oficina' => 'Cierre Oficina',
    'observaciones' => 'Observaciones',
    'id_municipio' => 'Municipio',
    'id_localidad' => 'Localidad',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
