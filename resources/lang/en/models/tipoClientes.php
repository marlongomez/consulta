<?php

return array (
  'singular' => 'Tipo_Clientes',
  'plural' => 'Tipo_Clientes',
  'fields' => 
  array (
    'id_tipo_cliente' => 'Id Tipo Cliente',
    'nombre_tipo' => 'Nombre Tipo',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
