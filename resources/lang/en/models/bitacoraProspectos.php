<?php

return array (
  'singular' => 'Bitacora_Prospecto',
  'plural' => 'Bitacora_Prospectos',
  'fields' => 
  array (
    'id_bitacora_prospecto' => 'Id Bitacora Prospecto',
    'id_prospecto' => 'Id Prospecto',
    'fecha' => 'Fecha',
    'observaciones' => 'Observaciones',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
