<?php

return array (
  'singular' => 'Prospecto',
  'plural' => 'Prospectos',
  'fields' => 
  array (
    'id_prospecto' => 'Id Prospecto',
    'nombre_prospecto' => 'Nombre Prospecto',
    'rfc_prospecto' => 'Rfc Prospecto',
    'telefono' => 'Telefono',
    'id_dependencia' => 'Id Dependencia',
    'id_oficina' => 'Id Oficina',
    'correo_electronico' => 'Correo Electronico',
    'id_tipo_cliente' => 'Id Tipo Cliente',
    'id_municipio' => 'Id Municipio',
    'id_localidad' => 'Id Localidad',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
