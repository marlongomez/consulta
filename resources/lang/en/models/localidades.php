<?php

return array (
  'singular' => 'Localidades',
  'plural' => 'Localidades',
  'fields' => 
  array (
    'id_localidad' => 'Id Localidad',
    'id_municipio' => 'Id Municipio',
    'nombre_localidad' => 'Nombre Localidad',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
