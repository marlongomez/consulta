<?php

return array (
  'singular' => 'Municipio',
  'plural' => 'Municipios',
  'fields' => 
  array (
    'id_municipio' => 'Id Municipio',
    'nombre' => 'Nombre',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
