<?php

return array (
  'singular' => 'Estados',
  'plural' => 'Estados',
  'fields' => 
  array (
    'id' => 'Id',
    'estado' => 'Estado',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
