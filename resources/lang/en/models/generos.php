<?php

return array (
  'singular' => 'Genero',
  'plural' => 'Generos',
  'fields' => 
  array (
    'id_genero' => 'Id Genero',
    'nombre' => 'Nombre',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
