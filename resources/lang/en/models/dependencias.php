<?php

return array (
  'singular' => 'Dependencias',
  'plural' => 'Dependencias',
  'fields' => 
  array (
    'id_dependencia' => 'Id Dependencia',
    'nombre_dependencia' => 'Nombre Dependencia',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
