<?php

return [

    'add_new'      => 'Agregar',
    'cancel'       => 'Cancelar',
    'save'         => 'Guardar',
    'edit'         => 'Editar',
    'detail'       => 'Detalles',
    'back'         => 'Regresar',
    'action'       => 'Acciones',
    'id'           => 'Id',
    'created_at'   => 'Created At',
    'updated_at'   => 'Updated At',
    'deleted_at'   => 'Deleted At',
    'are_you_sure' => '¿Está seguro que desea eliminar el registro?',
];
