<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor inténtelo de nuevo en :seconds segundos.',
    'login'=>[
        'title'=>'Iniciar Sesión',
        'forgot_password' => 'Olvidé mi contraseña'
    ],
    'remember_me'=>'Recordar',
    'sign_in'=>'Iniciar Sesión',
    'email'=>'Correo',
    'password'=>'Contraseña',
    'confirm_password'=>'Confirmar Contraseña',
    'full_name'=>'Nombre Completo',
    'register'=>'Registrarse',
    'forgot_password'=>[
        'title'=>'Ingrese el correo electrónico para restablecer la contraseña',
        'send_pwd_reset'=>'Enviar reinicio de Contraseña'
    ],
    'reset_password'=>[
        'title'=>'Restablecer Contraseña',
        'reset_pwd_btn'=>'Boton de Restablecer Contraseña'
    ],
    'registration'=>[
        'title'=>'Registrar Usuario',
        'i_agree'=>'Aceptar',
        'terms'=>'Terminos',
        'have_membership'=>'Ya Tengo Cuenta',

    ],
    'app'=>[
        'member_since'=>'Miembro desde',
        'profile'=>'Perfil',
        'online'=>'En linea',
        'search'=>'Buscar',
        'genders'=>'Géneros',
        'levels'=>'Niveles',
        'users'=>'Usuarios',
        'list_genders'=>'Listado de Géneros',
        'list_nivels'=>'Listado de Niveles',
        'list_users'=>'Listado de Usuarios',
        'list_qrs'=>'Listado de Codigos Qr',
        'expedientes'=>'Expedientes',
        'create'       => 'Crear',
        'export'       => 'Exportar',
        'excel'       => 'Excel',
        'pdf'       => 'PDF',
        'print'        => 'Imprimir',
        'reset'        => 'Reiniciar',
        'reload'       => 'Recargar',

    ],
    'sign_out' =>'Cerrar Sesión',

];
