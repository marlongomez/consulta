<?php

return array (
  'singular' => 'Bitacora del cliente Prospecto',
  'plural' => 'Bitacoras de los clientes Prospectos',
  'fields' => 
  array (
    'id_bitacora_prospecto' => 'Id Bitacora Prospecto',
    'id_prospecto' => 'Id Prospecto',
    'fecha' => 'Fecha',
    'observaciones' => 'Observaciones',
    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
  ),
);
