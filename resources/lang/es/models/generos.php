<?php

return array (
  'singular' => 'Genero',
  'plural' => 'Generos',
  'fields' => 
  array (
    'id_genero' => 'Id Genero',
    'nombre' => 'Nombre',
    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
  ),
);
