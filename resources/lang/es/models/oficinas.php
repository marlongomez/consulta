<?php

return array (
  'singular' => 'Oficinas',
  'plural' => 'Oficinas',
  'fields' => 
  array (
    'id_oficina' => 'Id Oficina',
    'id_dependencia' => 'Dependencia',
    'nombre_oficina' => 'Nombre de la Oficina',
    'direccion_oficina' => 'Direccion Oficina',
    'apertura_oficina' => 'Apertura de la Oficina',
    'cierre_oficina' => 'Cierre de la Oficina',
    'observaciones' => 'Observaciones',
    'id_municipio' => 'Municipio',
    'id_localidad' => 'Localidad',
    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
  ),
);
