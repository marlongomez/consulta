<?php

return array (
  'singular' => 'Bitacora',
  'plural' => 'Bitacoras',
  'fields' => 
  array (
    'id_bitacora' => 'Id Bitacora',
    'id_cliente' => 'Id Cliente',
    'fecha' => 'Fecha',
    'observaciones' => 'Observaciones',
    'ultima_venta' => 'Ultima Venta',
    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
  ),
);
