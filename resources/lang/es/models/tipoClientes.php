<?php

return array (
  'singular' => 'Tipo Clientes',
  'plural' => 'Tipo Clientes',
  'fields' => 
  array (
    'id_tipo_cliente' => 'Id Tipo Cliente',
    'nombre_tipo' => 'Tipo Cliente',
    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
  ),
);
