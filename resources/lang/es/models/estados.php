<?php

return array (
  'singular' => 'Estado',
  'plural' => 'Estados',
  'fields' => 
  array (
    'id' => 'Id',
    'estado' => 'Estado',
    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
  ),
);
