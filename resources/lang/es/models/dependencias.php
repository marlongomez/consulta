<?php

return array (
  'singular' => 'Dependencias',
  'plural' => 'Dependencias',
  'fields' => 
  array (
    'id_dependencia' => 'Id Dependencia',
    'nombre_dependencia' => 'Nombre Dependencia',
    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
  ),
);
