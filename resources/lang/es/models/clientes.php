<?php

return array (
  'singular' => 'Clientes',
  'plural' => 'Clientes',
  'fields' => 
  array (
    'id_cliente' => 'Id cliente',
    'nombre_cliente' => 'Nombre del cliente',
    'rfc_cliente' => 'RFC',
    'telefono' => 'Telefono',
    'id_dependencia' => 'Dependencia',
    'id_oficina' => 'Oficina',
    'correo_electronico' => 'Correo electronico',
    'id_tipo_cliente' => 'Tipo de cliente',
    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
  ),
);
