<?php

return array (
  'singular' => 'Municipio',
  'plural' => 'Municipios',
  'fields' => 
  array (
    'id_municipio' => 'Municipio',
    'nombre' => 'Nombre',
    'created_at' => 'Creado el',
    'updated_at' => 'Actualizado el',
  ),
);
