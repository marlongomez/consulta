<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('generos', App\Http\Controllers\API\GeneroAPIController::class);

Route::resource('municipios', App\Http\Controllers\API\MunicipioAPIController::class);

Route::resource('localidades', App\Http\Controllers\API\LocalidadesAPIController::class);

Route::resource('dependencias', App\Http\Controllers\API\DependenciasAPIController::class);

Route::resource('oficinas', App\Http\Controllers\API\OficinasAPIController::class);

Route::resource('tipo__clientes', App\Http\Controllers\API\Tipo_ClientesAPIController::class);

Route::resource('clientes', App\Http\Controllers\API\ClientesAPIController::class);

Route::resource('bitacoras', App\Http\Controllers\API\BitacoraAPIController::class);

Route::resource('prospectos', App\Http\Controllers\API\ProspectoAPIController::class);

Route::resource('bitacora__prospectos', App\Http\Controllers\API\Bitacora_ProspectoAPIController::class);