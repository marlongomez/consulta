<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('generos', App\Http\Controllers\GeneroController::class);

Route::resource('municipios', App\Http\Controllers\MunicipioController::class);

Route::resource('localidades', App\Http\Controllers\LocalidadesController::class);

Route::get('localidades/get-localidades/{id_mun}', 'OficinasController@getLocalidades');

Route::resource('dependencias', App\Http\Controllers\DependenciasController::class);

Route::resource('oficinas', App\Http\Controllers\OficinasController::class);

Route::resource('tipoClientes', App\Http\Controllers\Tipo_ClientesController::class);

Route::resource('clientes', App\Http\Controllers\ClientesController::class);

Route::resource('bitacoras', App\Http\Controllers\BitacoraController::class);

Route::get('bitacoras/index/{id_cliente}',[App\Http\Controllers\BitacoraController::class, 'index']
)->name('bitacoras.index')->middleware('verified');

Route::get('bitacoras/index/{id_cliente}/create',[App\Http\Controllers\BitacoraController::class, 'create']
)->name('bitacoras.create')->middleware('verified');

/*Route::get('bitacoras/edit/{id_cliente}',[App\Http\Controllers\BitacoraController::class, 'edit']
)->name('bitacoras.edit')->middleware('verified');*/

Route::resource('prospectos', App\Http\Controllers\ProspectoController::class);

Route::resource('bitacoraProspectos', App\Http\Controllers\Bitacora_ProspectoController::class);

Route::get('bitacoraProspectos/index/{id_prospecto}',[App\Http\Controllers\Bitacora_ProspectoController::class, 'index']
)->name('bitacoraProspectos.index')->middleware('verified');

Route::get('bitacoraProspectos/index/{id_prospecto}/create',[App\Http\Controllers\Bitacora_ProspectoController::class, 'create']
)->name('bitacoraProspectos.create')->middleware('verified');

//rutas para los reportes
Route::resource('reportes', App\Http\Controllers\ReporteController::class);

Route::get('reportes/municipio/', [App\Http\Controllers\ReporteController::class, 'show'])
         ->name('reportes.municipio')->middleware('verified');

Route::post('reportes/municipio/municipioPDF', [App\Http\Controllers\ReporteController::class, 'municipioPDF'])
         ->name('reportes.municipioPDF')->middleware('verified');


Route::resource('reportesDependencias', App\Http\Controllers\ReporteDependenciasController::class);

Route::get('reportesDependencias/dependencias/', [App\Http\Controllers\ReporteDependenciasController::class, 'show'])
         ->name('reportesDependencias.dependencias')->middleware('verified');

Route::post('reportesDependencias/dependencias/dependenciaPDF', [App\Http\Controllers\ReporteDependenciasController::class, 'dependenciaPDF'])
         ->name('reportesDependencias.dependenciaPDF')->middleware('verified');
