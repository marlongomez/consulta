<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitacoraTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora', function (Blueprint $table) {
            $table->increments('id_bitacora');
            $table->integer('id_cliente')->unsigned()->nullable();
            $table->date('fecha')->nullable();
            $table->text('observaciones')->nullable();
            $table->text('ultima_venta')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_cliente')->references('id_cliente')->on('clientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bitacora');
    }
}
