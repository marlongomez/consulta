<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitacoraProspectoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora_prospecto', function (Blueprint $table) {
            $table->increments('id_bitacora_prospecto');
            $table->integer('id_prospecto')->unsigned()->nullable();
            $table->timestamp('fecha')->nullable();
            $table->text('observaciones')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_prospecto')->references('id_prospecto')->on('prospecto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bitacora_prospecto');
    }
}
