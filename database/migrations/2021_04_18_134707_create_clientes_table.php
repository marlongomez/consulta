<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id_cliente');
            $table->text('nombre_cliente');
            $table->string('rfc_cliente')->unique()->nullable();
            $table->text('telefono')->nullable();
            $table->integer('id_dependencia');
            $table->integer('id_oficina');
            $table->text('correo_electronico')->nullable();
            $table->integer('id_tipo_cliente');
            $table->integer('id_municipio')->unsigned()->nullable();
            $table->integer('id_localidad')->unsigned()->nullable();
            $table->text('agendado')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_municipio')->references('id_municipio')->on('municipio');
            $table->foreign('id_localidad')->references('id_localidad')->on('localidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
