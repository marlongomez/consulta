<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOficinasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oficinas', function (Blueprint $table) {
            $table->increments('id_oficina');
            $table->integer('id_dependencia')->unsigned();
            $table->text('nombre_oficina')->nullable();
            $table->text('direccion_oficina')->nullable();
            $table->text('apertura_oficina')->nullable();
            $table->text('cierre_oficina')->nullable();
            $table->text('observaciones')->nullable();
            $table->integer('id_municipio')->unsigned()->nullable();
            $table->integer('id_localidad')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_dependencia')->references('id_dependencia')->on('dependencias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oficinas');
    }
}
