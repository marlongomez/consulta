<?php

namespace Database\Factories;

use App\Models\Localidades;
use Illuminate\Database\Eloquent\Factories\Factory;

class LocalidadesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Localidades::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_municipio' => $this->faker->randomDigitNotNull,
        'nombre_localidad' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
