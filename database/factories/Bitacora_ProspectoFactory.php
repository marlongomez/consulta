<?php

namespace Database\Factories;

use App\Models\Bitacora_Prospecto;
use Illuminate\Database\Eloquent\Factories\Factory;

class Bitacora_ProspectoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bitacora_Prospecto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_prospecto' => $this->faker->randomDigitNotNull,
        'fecha' => $this->faker->word,
        'observaciones' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
