<?php

namespace Database\Factories;

use App\Models\Oficinas;
use Illuminate\Database\Eloquent\Factories\Factory;

class OficinasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Oficinas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_dependencia' => $this->faker->randomDigitNotNull,
        'nombre_oficina' => $this->faker->text,
        'direccion_oficina' => $this->faker->text,
        'apertura_oficina' => $this->faker->text,
        'cierre_oficina' => $this->faker->text,
        'observaciones' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
