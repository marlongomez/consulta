<?php

namespace Database\Factories;

use App\Models\Clientes;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Clientes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre_cliente' => $this->faker->text,
        'rfc_cliente' => $this->faker->text,
        'telefono' => $this->faker->text,
        'id_dependencia' => $this->faker->randomDigitNotNull,
        'id_oficina' => $this->faker->randomDigitNotNull,
        'correo_electronico' => $this->faker->text,
        'id_tipo_cliente' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
